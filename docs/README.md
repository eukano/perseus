# Perseus

Perseus is a bare-bones infrastructure management system.

## Components

  * Agent - runs on managed node
  * Conductor - tells agents to launch tasks
  * Tasks - exist as source projects
  * Builder - runs in the cloud as a lambda/serverless function
  * Broker - brokers communications between conductors and agents
  
## Process Flow

  * Ping
    1) Conductor queries for active Agents via Broker
    2) Active agents respond with their status
  * Execute
    1) Conductor instructs Agents to execute a Task via Broker
    2) Agent requests task from Builder
    3) [GitLab] Builder retrieves compiled task from CI artifacts, or triggers a new build and returns "try later".
    4) Agent executes task and returns results to Conductor