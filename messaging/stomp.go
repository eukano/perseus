package messaging

import (
	"context"
	"crypto/tls"
	"encoding/json"
	"io"
	"log"
	"net"
	"time"

	"github.com/go-stomp/stomp"
	"gitlab.com/eukano/perseus/internal/tls_debug"
	"gitlab.com/eukano/perseus/trust"
	"golang.org/x/xerrors"
)

type StompConfiguration struct {
	Server    string        `toml:"server,omitempty"`
	UseTLS    bool          `toml:"use-tls,omitempty"`
	Username  string        `toml:"username,omitempty"`
	Password  string        `toml:"password,omitempty"`
	Heartbeat time.Duration `toml:"heartbeat,omitempty"`
}

func (c *StompConfiguration) Validate() (*StompBroker, error) {
	if c.Server == "" {
		return nil, xerrors.Errorf("[broker.stomp] server must be specified")
	}

	return &StompBroker{c}, nil
}

func IsStompClosed(err error) bool {
	return err == stomp.ErrClosedUnexpectedly || err == stomp.ErrAlreadyClosed
}

type StompBroker struct {
	config *StompConfiguration
}

func (b *StompBroker) Connect(ctx context.Context, autoreconnect bool) (BrokerConnection, error) {
	conn := &StompConnection{
		config: b.config,
		conn:   make(chan stompConnectionState),
	}
	conn.context, conn.cancel = context.WithCancel(ctx)

	if autoreconnect {
		go conn.runPersistent()
		return conn, nil
	} else {
		return conn, conn.runOnce()
	}
}

type StompConnection struct {
	config  *StompConfiguration
	context context.Context
	cancel  context.CancelFunc
	conn    chan stompConnectionState
}

type stompConnectionState struct {
	conn   *stomp.Conn
	cancel context.CancelFunc
}

func (c *StompConnection) connect(ctx context.Context) (*stomp.Conn, error) {
	var sock io.ReadWriteCloser
	var err error
	if c.config.UseTLS {
		sock, err = tls.Dial("tcp", c.config.Server, tls_debug.Configure(ctx, nil))
	} else {
		sock, err = net.Dial("tcp", c.config.Server)
	}
	if err != nil {
		return nil, err
	}

	opts := []func(*stomp.Conn) error{}
	if c.config.Username != "" || c.config.Password != "" {
		opts = append(opts, stomp.ConnOpt.Login(c.config.Username, c.config.Password))
	}
	if c.config.Heartbeat > 0 {
		opts = append(opts, stomp.ConnOpt.HeartBeat(c.config.Heartbeat, c.config.Heartbeat))
	}

	conn, err := stomp.Connect(sock, opts...)
	if err != nil {
		return nil, err
	}
	go func() { <-ctx.Done(); conn.Disconnect() }()

	return conn, nil
}

func (c *StompConnection) runOnce() error {
	conn, err := c.connect(c.context)
	if err != nil {
		return xerrors.Errorf("failed to connect to broker: %w", err)
	}

	go func() {
		defer close(c.conn)
		defer c.cancel()

		sctx, scancel := context.WithCancel(context.Background())
		for {
			select {
			case <-c.context.Done():
				return

			case <-sctx.Done():
				return

			case c.conn <- stompConnectionState{conn, scancel}:
			}
		}
	}()

	return nil
}

func (c *StompConnection) runPersistent() {
	defer close(c.conn)
	defer c.cancel()

	var tries int
	for {
		select {
		case <-c.context.Done():
			return

		default:
			t := time.Now()

			conn, err := c.connect(c.context)
			if err != nil {
				log.Printf("Failed to connect to broker: %v", err)
			} else {
				sctx, scancel := context.WithCancel(context.Background())
				ok := true

				for ok {
					select {
					case <-c.context.Done():
						scancel()
						return

					case <-sctx.Done():
						ok = false

					case c.conn <- stompConnectionState{conn, scancel}:
					}
				}
				scancel()
			}

			if time.Since(t) > time.Hour {
				tries = 0
				continue
			} else {
				tries++
			}

			switch {
			case tries < 5:
				time.Sleep(5 * time.Second)

			case tries < 10:
				time.Sleep(1 * time.Minute)

			default:
				time.Sleep(10 * time.Minute)
			}
		}
	}
}

func (c *StompConnection) send(dest, replyTo string, msg *trust.SignedMessage) error {
	raw, err := json.Marshal(msg)
	if err != nil {
		return err
	}

	for {
		state, ok := <-c.conn
		if !ok {
			return xerrors.Errorf("context canceled")
		}

		err := state.conn.Send(dest, "application/json", raw, stomp.SendOpt.Header("Reply-To", replyTo))
		if IsStompClosed(err) {
			state.cancel()
			continue
		}
		return err
	}
}

func (c *StompConnection) subscribe(dest string) *StompSubscription {
	sub := &StompSubscription{
		conn:    c,
		channel: make(chan BrokerMessage),
	}
	sub.context, sub.cancel = context.WithCancel(c.context)

	go sub.run(dest)
	return sub
}

func (c *StompConnection) SendToQueue(name, replyTo string, msg *trust.SignedMessage) error {
	if name == "" {
		return xerrors.Errorf("queue name must not be empty")
	}
	return c.send(name, replyTo, msg)
}

func (c *StompConnection) SendToTopic(name, replyTo string, msg *trust.SignedMessage) error {
	if name == "" {
		return xerrors.Errorf("topic name must not be empty")
	}
	return c.send("/topic/"+name, replyTo, msg)
}

func (c *StompConnection) SubscribeToQueue(name string) (BrokerSubscription, error) {
	if name == "" {
		return nil, xerrors.Errorf("queue name must not be empty")
	}
	return c.subscribe(name), nil
}

func (c *StompConnection) SubscribeToTopic(name string) (BrokerSubscription, error) {
	if name == "" {
		return nil, xerrors.Errorf("topic name must not be empty")
	}
	return c.subscribe("/topic/" + name), nil
}

func (c *StompConnection) Close() {
	c.cancel()
}

type StompSubscription struct {
	conn    *StompConnection
	context context.Context
	cancel  context.CancelFunc
	channel chan BrokerMessage
}

func (s *StompSubscription) run(dest string) {
	defer close(s.channel)
	defer s.cancel()

	for {
		select {
		case <-s.context.Done():
			return

		case state, ok := <-s.conn.conn:
			if !ok {
				return
			}

			sub, err := state.conn.Subscribe(dest, stomp.AckAuto)
			if IsStompClosed(err) {
				state.cancel()
				continue
			}
			if err != nil {
				log.Printf("Failed to subscribe to %s: %v", dest, err)
				return
			}

			s.handle(sub)
			state.cancel()
		}
	}
}

func (s *StompSubscription) handle(sub *stomp.Subscription) {
	defer sub.Unsubscribe()

	for {
		msg := &StompMessage{
			conn: s.conn,
		}

		var ok bool
		select {
		case <-s.context.Done():
			return

		case msg.Message, ok = <-sub.C:
			if !ok {
				return
			}
		}

		switch msg.Message.ContentType {
		case "":
			log.Printf("Received invalid message: unspecified content type")
			continue

		case "application/json":

		default:
			log.Printf("Received invalid message: unsupported content type: '%s'", msg.Message.ContentType)
			continue
		}

		msg.body = new(trust.SignedMessage)
		err := msg.body.Unmarshal(msg.Message.Body)
		if err != nil {
			log.Printf("Failed to decode message: %v", err)
			continue
		}

		select {
		case <-s.context.Done():
			return

		case s.channel <- msg:
		}
	}
}

func (s *StompSubscription) Receive() (BrokerMessage, bool) {
	select {
	case <-s.context.Done():
		return nil, false

	case msg := <-s.channel:
		return msg, true
	}
}

func (s *StompSubscription) Close() {
	s.cancel()
}

type StompMessage struct {
	*stomp.Message
	conn *StompConnection
	body *trust.SignedMessage
}

func (s *StompMessage) Body() *trust.SignedMessage { return s.body }

func (s *StompMessage) Send(resp *trust.SignedMessage) error {
	reply := s.Message.Header.Get("Reply-To")
	if reply == "" {
		return xerrors.Errorf("missing reply-to header")
	}

	return s.conn.send(reply, "application/json", resp)
}
