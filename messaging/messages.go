package messaging

type Solicitation struct {
	WillConnect, WantMachineInfo bool
}

type Announcement struct {
	Name        string
	Connect     string
	Machine     *MachineInfo
	Certificate []byte
}

type MachineInfo struct {
	OS, Arch string
}

type Invitation struct {
	Name, For   string
	Certificate []byte
}

type Execute struct {
	Task string
}

type Result struct {
	Output     []string
	ReturnCode int
}
