package messaging

import (
	"context"

	"gitlab.com/eukano/perseus/trust"
)

type Broker interface {
	Connect(context context.Context, autoreconnect bool) (BrokerConnection, error)
}

type BrokerConnection interface {
	SendToQueue(name, replyTo string, msg *trust.SignedMessage) error
	SendToTopic(name, replyTo string, msg *trust.SignedMessage) error
	SubscribeToQueue(name string) (BrokerSubscription, error)
	SubscribeToTopic(name string) (BrokerSubscription, error)
	Close()
}

type BrokerSubscription interface {
	Receive() (BrokerMessage, bool)
	Close()
}

type BrokerMessage interface {
	Body() *trust.SignedMessage
	Send(*trust.SignedMessage) error
}

// topic := "/topic/perseus-announce"
// if b.config.Topic != "" {
// 	topic = "/topic/" + b.config.Topic
// }
