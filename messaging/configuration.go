package messaging

import "golang.org/x/xerrors"

type BrokerKind string

const (
	DefaultBrokerKind BrokerKind = ""
	StompBrokerKind   BrokerKind = "stomp"
)

type BrokerConfiguration struct {
	implementation Broker

	Kind BrokerKind `toml:"kind,omitempty"`

	Stomp *StompConfiguration `toml:"stomp,omitempty"`
}

func (c *BrokerConfiguration) Implementation() Broker { return c.implementation }

func (c *BrokerConfiguration) Validate() error {
	switch c.Kind {
	case DefaultBrokerKind:
		return xerrors.Errorf("[brokers] broker kind must be specified")

	case StompBrokerKind:
		if c.Stomp == nil {
			return xerrors.Errorf("[brokers] stomp broker requires additional configuration in the [brokers.stomp] section")
		}
		if broker, err := c.Stomp.Validate(); err != nil {
			return err
		} else {
			c.implementation = broker
		}
		return nil

	default:
		return xerrors.Errorf("[broker] '%s' is not a supported broker kind", c.Kind)
	}
}
