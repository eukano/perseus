package admin

import (
	"gitlab.com/eukano/perseus/internal/configure"
	"gitlab.com/eukano/perseus/messaging"
	"gitlab.com/eukano/perseus/signing"
	"gitlab.com/eukano/perseus/trust"
)

func FindConfiguration() (*Configuration, error) {
	var c Configuration
	return &c, configure.Find("admin.toml", &c)
}

func LoadConfiguration(path string) (*Configuration, error) {
	var c Configuration
	return &c, configure.Load(path, &c)
}

func SaveConfiguration(path string, config *Configuration) error {
	return configure.Save(path, config)
}

type Configuration struct {
	configure.ConfigFile

	Subject         *trust.SubjectConfiguration `toml:"subject,omitempty"`
	Trust           *trust.Configuration        `toml:"trust,omitempty"`
	Brokers         []*BrokerConfiguration      `toml:"brokers,omitempty"`
	SignatureStores signing.ConfigurationSet    `toml:"signature-stores,omitempty"`
}

type BrokerConfiguration struct {
	messaging.BrokerConfiguration

	AgentTopic string `toml:"agent-topic,omitempty"`
	ReplyQueue string `toml:"reply-queue,omitempty"`
}

func (c *Configuration) Save() error { return SaveConfiguration(c.LoadPath(), c) }

func (c *Configuration) Validate() error {
	if c.Subject == nil {
		c.Subject = new(trust.SubjectConfiguration)
	}
	if err := c.Subject.Validate(false); err != nil {
		return err
	}

	if c.Trust == nil {
		c.Trust = new(trust.Configuration)
	}
	if err := c.Trust.Validate(c.Subject.Get(), c.TrustOptions()); err != nil {
		return err
	}

	if len(c.Brokers) == 0 {
		c.Brokers = []*BrokerConfiguration{}
	}
	for _, b := range c.Brokers {
		if err := b.Validate(); err != nil {
			return err
		}
	}

	if len(c.SignatureStores) == 0 {
		c.SignatureStores = signing.ConfigurationSet{}
	}
	if err := c.SignatureStores.Validate(); err != nil {
		return err
	}

	return nil
}
