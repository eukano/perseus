package main

import (
	"bytes"
	"fmt"
	"io"
	"net"
	"os"
)

func main() {
	ln, err := net.Listen("tcp", "127.0.0.1:6789")
	if err != nil {
		panic(err)
	}

	conn, err := ln.Accept()
	if err != nil {
		panic(err)
	}
	defer conn.Close()
	fmt.Println("got connection, closing listener: ", conn.RemoteAddr())

	err = ln.Close()
	if err != nil {
		panic(err)
	}

	fmt.Println("dumping")
	var buf [4096]byte
	for {
		n, err := conn.Read(buf[:])
		if err == io.EOF {
			return
		}
		if err != nil {
			panic(err)
		}

		_, err = bytes.NewBuffer(buf[:n]).WriteTo(os.Stdout)
		if err != nil {
			panic(err)
		}
	}
}
