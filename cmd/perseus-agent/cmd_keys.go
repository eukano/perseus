package main

import (
	"fmt"
	"log"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/eukano/perseus/trust"
)

func runKeys(cmd *cobra.Command, fingerprint, all bool, accept, reject, delete []string) {
	var didSomething, pendingDirty, trustedDirty bool
	var pending = config.Trust.PendingStore()
	var trusted = config.Trust.TrustedStore()

	ensureCert := func() {
		err := config.Trust.EnsureOwnCertificate(config.Subject.Get())
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to create a certificate: %v\n", err)
			os.Exit(1)
		}
	}

	if fingerprint {
		didSomething = true

		ensureCert()
		fmt.Println(config.Trust.MustGetOwnKey().Public().Fingerprint())
	}

	for _, fp := range accept {
		didSomething = true

		if fp == "" {
			fmt.Fprintf(os.Stderr, "Ignoring --accept with empty argument\n")
			continue
		}

		cert, key, ok := pending.Locate(fp)
		if !ok {
			fmt.Fprintf(os.Stderr, "No pending key or cert with fingerprint %s\n", fp)
			continue
		}

		if cert != nil {
			pending.DeleteCertificate(cert)
			trusted.AddCertificate(cert)
		} else {
			pending.DeletePublicKey(key)
			trusted.AddPublicKey(key)
		}
		pendingDirty = true
		trustedDirty = true
	}

	for _, fp := range reject {
		didSomething = true

		if fp == "" {
			fmt.Fprintf(os.Stderr, "Ignoring --reject with empty argument\n")
			continue
		}

		cert, key, ok := pending.Locate(fp)
		if !ok {
			fmt.Fprintf(os.Stderr, "No pending key or cert with fingerprint %s\n", fp)
			continue
		}

		if cert != nil {
			pending.DeleteCertificate(cert)
		}
		pending.DeletePublicKey(key)
		pendingDirty = true
	}

	for _, fp := range delete {
		didSomething = true

		if fp == "" {
			fmt.Fprintf(os.Stderr, "Ignoring --delete with empty argument\n")
			continue
		}

		cert, key, ok := trusted.Locate(fp)
		if !ok {
			fmt.Fprintf(os.Stderr, "No trusted key or cert with fingerprint %s\n", fp)
			continue
		}

		if cert != nil {
			trusted.DeleteCertificate(cert)
		}
		trusted.DeletePublicKey(key)
		trustedDirty = true
	}

	if pendingDirty {
		err := pending.Save()
		if err != nil {
			log.Printf("Failed to save pending key/cert store: %v\n", err)
		}
	}

	if trustedDirty {
		err := trusted.Save()
		if err != nil {
			log.Printf("Failed to save trusted key/cert store: %v\n", err)
		}
	}

	if all || !didSomething {
		stores := map[string]*trust.Store{"Pending": pending}
		if all {
			stores["Trusted"] = trusted
			stores["Cached"] = config.Trust.CachedStore()
		}

		for header, store := range stores {
			keys := store.PublicKeys()
			certs := store.Certificates()

			if len(keys) > 0 {
				fmt.Println(header + " public keys")
			}
			for _, key := range keys {
				fmt.Println("- " + key.Fingerprint())
			}

			if len(certs) > 0 {
				fmt.Println(header + " certificates")
			}
			for _, cert := range certs {
				fmt.Println("- (" + cert.Cert().Subject.CommonName + ") " + cert.Public().Fingerprint())
			}

			if len(keys)+len(certs) == 0 {
				fmt.Fprintf(os.Stderr, "No %s public keys or certificates\n\n", strings.ToLower(header))
			}
		}

		cmd.Usage()
	}
}
