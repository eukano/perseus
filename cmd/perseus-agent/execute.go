package main

import (
	"bytes"
	"crypto"
	"crypto/sha256"
	"encoding/base64"
	"io"
	"os/exec"
	"strings"

	"gitlab.com/eukano/perseus/signing"

	"gitlab.com/eukano/perseus/builder"
	"gitlab.com/eukano/perseus/internal/sys"
	"golang.org/x/xerrors"
)

func executeTask(resp *builder.Response, w io.Writer) error {
	f, err := sys.CreateTempTask()
	if err != nil {
		return xerrors.Errorf("failed to create temp file: %w", err)
	}

	_, err = bytes.NewBuffer(resp.Binary).WriteTo(f)
	if err != nil {
		return xerrors.Errorf("failed to write task to temp file: %w", err)
	}

	err = f.Close()
	if err != nil {
		return xerrors.Errorf("failed close temp file: %w", err)
	}

	digest := sha256.Sum256(resp.Binary)
	record := &signing.Record{
		Digest:     "SHA256:" + base64.RawStdEncoding.EncodeToString(digest[:]),
		Signatures: map[string]string{},
	}

	err = config.SignatureStores.Stores().Get(record)
	if err == signing.ErrRecordNotFound {
		return xerrors.Errorf("no signature found for binary (digest: %s)", record.Digest)
	}
	if err != nil {
		return err
	}

	if !verifySignature(digest[:], record) {
		return xerrors.Errorf("no signature found for binary (digest: %s)", record.Digest)
	}

	cmd := exec.Command(f.Name())
	cmd.Stdout = w
	cmd.Stderr = w
	return cmd.Run()
}

func verifySignature(digest []byte, record *signing.Record) bool {
	for fp, sig := range record.Signatures {
		_, key, ok := config.Trust.TrustedStore().Locate(fp)
		if !ok {
			continue
		}

		i := strings.IndexRune(sig, ':')
		if i < 0 {
			continue
		}
		if sig[:i] != key.Type() {
			continue
		}

		signature, err := base64.RawStdEncoding.DecodeString(sig[i+1:])
		if err != nil {
			continue
		}

		if key.Verify(digest, signature, crypto.SHA256) == nil {
			return true
		}
	}

	return false
}
