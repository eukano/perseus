package main

import (
	"bufio"
	"bytes"
	"context"
	"crypto/tls"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"os"
	"runtime"
	"strings"

	"github.com/kardianos/service"
	"gitlab.com/eukano/perseus/builder"
	"gitlab.com/eukano/perseus/messaging"
	"gitlab.com/eukano/perseus/trust"
	"golang.org/x/xerrors"
)

type Program struct {
	service service.Service
	context context.Context
	cancel  context.CancelFunc
}

func (p *Program) Start(svc service.Service) (err error) {
	err = config.Trust.EnsureOwnCertificate(config.Subject.Get())
	if err != nil {
		return err
	}

	p.service = svc
	p.context, p.cancel = context.WithCancel(context.Background())
	defer func() {
		if err != nil {
			p.cancel()
		}
	}()

	for _, b := range config.Brokers {
		topic := "perseus-announce"
		if b.Topic != "" {
			topic = b.Topic
		}

		conn, err := b.Implementation().Connect(p.context, true)
		if err != nil {
			return err
		}

		sub, err := conn.SubscribeToTopic(topic)
		if err != nil {
			return err
		}

		go p.handleBroker(sub)
	}

	return nil
}

func (p *Program) Stop(service.Service) error {
	p.cancel()
	return nil
}

func (p *Program) handleBroker(sub messaging.BrokerSubscription) {
	key := config.Trust.MustGetOwnKey()

	for {
		msg, ok := sub.Receive()
		if !ok {
			log.Println("Broker subscription shut down")
			return
		}

		body := msg.Body()
		if body.MessageType != "invitation" {
			if len(body.Signature) == 0 {
				log.Printf("Message is not signed")
				continue
			}

			if body.Fingerprint == "" {
				log.Printf("Message is missing fingerprint")
				continue
			}

			cert, _, _ := config.Trust.TrustedStore().Locate(body.Fingerprint)
			if cert == nil {
				log.Printf("No trusted certificate with fingerprint %s", body.Fingerprint)
				continue
			}

			err := body.Verify(cert.Public())
			if err != nil {
				log.Printf("Message signature invalid: %v", err)
				continue
			}
		}

		var rtype string
		var v interface{}
		var handle func() (interface{}, error)
		switch msg.Body().MessageType {
		case "solicitation":
			req := new(messaging.Solicitation)
			rtype = "announcement"
			v, handle = req, func() (interface{}, error) { return p.handleSolicitation(req, msg) }

		case "invitation":
			req := new(messaging.Invitation)
			rtype = "invitation"
			v, handle = req, func() (interface{}, error) { return p.handleInvitation(req, msg) }

		default:
			log.Printf("Unexpected message type: '%s'", body.MessageType)
			continue
		}

		go func() {
			err := json.Unmarshal(body.MessageData, v)
			if err != nil {
				log.Printf("Failed to unmarshal message payload: %v", err)
				return
			}

			resp, err := handle()
			if err != nil {
				log.Printf("Failed while handling %s: %v", msg.Body().MessageType, err)
				return
			}

			if resp == nil {
				return
			}

			raw, err := json.Marshal(resp)
			if err != nil {
				log.Printf("Failed to marshal message response: %v", err)
				return
			}

			sm := &trust.SignedMessage{
				MessageType: rtype,
				MessageData: raw,
			}
			err = sm.Sign(key)
			if err != nil {
				log.Printf("Failed to sign message response: %v", err)
				return
			}

			err = msg.Send(sm)
			if err != nil {
				log.Printf("Failed to send message response: %v", err)
			}
		}()
	}
}

func (p *Program) handleSolicitation(req *messaging.Solicitation, msg messaging.BrokerMessage) (*messaging.Announcement, error) {
	var resp messaging.Announcement
	resp.Name = config.Subject.CommonOrHostName()

	if req.WillConnect {
		name, err := config.Announce.Connect.Manager().Listen(p.handleConnection)
		if err != nil {
			return nil, xerrors.Errorf("failed to prepare connection: %w", err)
		}

		resp.Connect = name
	}
	if req.WantMachineInfo {
		resp.Machine = &messaging.MachineInfo{
			OS:   runtime.GOOS,
			Arch: runtime.GOARCH,
		}
	}

	return &resp, nil
}

func (p *Program) handleInvitation(req *messaging.Invitation, msg messaging.BrokerMessage) (*messaging.Invitation, error) {
	var resp messaging.Invitation
	resp.Name = config.Subject.CommonOrHostName()

	ownCert := config.Trust.MustGetOwnCert()
	if req.For != "" && req.For != ownCert.Public().Fingerprint() {
		return nil, nil
	}

	if len(req.Certificate) > 0 {
		cert, err := trust.UnmarshalCertificate(req.Certificate)
		if err != nil {
			return nil, xerrors.Errorf("failed to unmarshal certificate from invitation: %w", err)
		}

		fp := cert.Public().Fingerprint()
		stores := config.Trust.Stores()
		_, _, i := stores.Locate(fp)
		if i < 0 {
			s := config.Trust.PendingStore()
			s.AddCertificate(cert)
			err = s.Save()
			if err != nil {
				return nil, xerrors.Errorf("failed to save certificate to pending trust store: %w", err)
			}
		} else {
			log.Printf("already have certificate %s in %s\n", fp, stores[i].Path())
		}

		resp.For = fp
	}

	resp.Certificate = ownCert.Cert().Raw
	return &resp, nil
}

func (p *Program) handleConnection(conn net.Conn, err error) {
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}

	if conn == nil {
		return
	}

	ownCert := config.Trust.MustGetOwnCert()
	ownKey := config.Trust.MustGetOwnKey()

	sconn := tls.Server(conn, &tls.Config{
		Certificates: []tls.Certificate{
			tls.Certificate{
				Certificate: [][]byte{ownCert.Cert().Raw},
				PrivateKey:  ownKey.Underlying(),
			},
		},
	})
	err = sconn.Handshake()
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		conn.Close()
		return
	}

	conn = sconn
	defer conn.Close()

	r := bufio.NewReader(conn)
	task, err := r.ReadString('\000')
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		return
	}
	task = task[:len(task)-1]

	parts := strings.Split(task, "@")
	if len(parts) != 2 {
		return
	}

	bld, ok := config.Builders.ForTask(parts[0])
	if !ok {
		bytes.NewBufferString(fmt.Sprintf("No builder configured for %q\n", task)).WriteTo(conn)
		return
	}

	resp, err := bld.RetrieveTask(parts[0], parts[1])
	if err != nil {
		bytes.NewBufferString(fmt.Sprintf("Failed to retrieve %q: %v\n", task, err)).WriteTo(conn)
		return
	}

	switch resp.Status {
	case builder.Invalid:
		bytes.NewBufferString(fmt.Sprintf("Failed to retrieve %q\n", task)).WriteTo(conn)
		for _, msg := range resp.Messages {
			bytes.NewBufferString(msg + "\n").WriteTo(conn)
		}

	case builder.Pending:
		bytes.NewBufferString(fmt.Sprintf("Building %q\n", task)).WriteTo(conn)
		for _, msg := range resp.Messages {
			bytes.NewBufferString(msg + "\n").WriteTo(conn)
		}

	case builder.Failed:
		bytes.NewBufferString(fmt.Sprintf("Failed to build %q\n", task)).WriteTo(conn)
		for _, msg := range resp.Messages {
			bytes.NewBufferString(msg + "\n").WriteTo(conn)
		}

	default:
		bytes.NewBufferString(fmt.Sprintf("Unknown status number: %d\n", resp.Status)).WriteTo(conn)
		for _, msg := range resp.Messages {
			bytes.NewBufferString(msg + "\n").WriteTo(conn)
		}

	case builder.Success:
		err := executeTask(resp, conn)
		if err != nil {
			bytes.NewBufferString(fmt.Sprintf("Failed to execute %q: %v\n", task, err)).WriteTo(conn)
		}
	}
}
