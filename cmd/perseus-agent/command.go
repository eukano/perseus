package main

import (
	"errors"
	"fmt"
	"os"

	"github.com/kardianos/service"
	"github.com/spf13/cobra"
	"gitlab.com/eukano/perseus/agent"
	"gitlab.com/eukano/perseus/internal/tls_debug"
)

var config *agent.Configuration
var requireConfig, validateConfig bool
var tlsKeyLogPath string

func requireValidConfig(cmd *cobra.Command, args []string) error {
	requireConfig = true
	validateConfig = true
	return rootCmd.PersistentPreRunE(cmd, args)
}

var rootCmd = func() *cobra.Command {
	var configPath string

	cmd := &cobra.Command{
		Use:   "perseus-agent",
		Short: "Perseus Agent is the Perseus CMS agent",
		Long:  "Perseus Agent is the agent for the Perseus infrastructure configuration management system. Documentation is available at https://perseus-cms.io.",
		Run:   func(cmd *cobra.Command, _ []string) { cmd.Help() },

		PersistentPreRunE: func(*cobra.Command, []string) error {
			var err error
			if configPath == "" {
				config, err = agent.FindConfiguration()
			} else {
				config, err = agent.LoadConfiguration(configPath)
			}
			if !requireConfig && errors.Is(err, os.ErrNotExist) {
				config = new(agent.Configuration)
			} else if err != nil {
				return err
			}

			if !validateConfig {
				return nil
			}
			return config.Validate()
		},
	}

	cmd.PersistentFlags().SortFlags = false
	cmd.PersistentFlags().StringVarP(&configPath, "config", "C", "", "Configuration file")

	tls_debug.AddFlags(cmd)
	return cmd
}()

var keysCmd = func() interface{} {
	var fingerprint, all bool
	var accept, reject, delete []string

	cmd := &cobra.Command{
		Use:   "keys",
		Short: "Manage keys",
		Run:   func(cmd *cobra.Command, args []string) { runKeys(cmd, fingerprint, all, accept, reject, delete) },

		PersistentPreRunE: requireValidConfig,
	}
	rootCmd.AddCommand(cmd)

	cmd.Flags().SortFlags = false
	cmd.Flags().BoolVarP(&fingerprint, "fingerprint", "f", false, "Display agent fingerprint")
	cmd.Flags().BoolVar(&all, "all", false, "List all pending, trusted, and cached keys and certificates")
	cmd.Flags().StringArrayVarP(&accept, "accept", "a", nil, "Accept key by `fingerprint` (can be repeated)")
	cmd.Flags().StringArrayVarP(&reject, "reject", "r", nil, "Reject key by `fingerprint` (can be repeated)")
	cmd.Flags().StringArrayVarP(&delete, "delete", "d", nil, "Delete key by `fingerprint` (can be repeated)")

	return struct{}{}
}()

var serviceCmds = func() interface{} {
	serviceConfig := &service.Config{
		Name:        "perseus-agent",
		DisplayName: "Perseus Agent",
		Description: "The Perseus Configuration Management System agent. Documentation is available at https://perseus-cms.io.",
	}

	initService := func() (service.Service, error) {
		return service.New(new(Program), serviceConfig)
	}

	serviceCmd := func(action func(service.Service) error) func(*cobra.Command, []string) {
		return func(*cobra.Command, []string) {
			svc, err := initService()
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}

			err = action(svc)
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}
		}
	}

	rootCmd.AddCommand(&cobra.Command{
		Use:   "install",
		Short: "Install the perseus-agent service",

		PersistentPreRunE: requireValidConfig,

		Run: func(*cobra.Command, []string) {
			abs, err := config.AbsolutePath()
			if validateConfig && err != nil {
				fmt.Fprintf(os.Stderr, "Could not absolute path to configuration: %v\n", err)
				os.Exit(1)
			}

			serviceConfig.Arguments = []string{"run", "-C", abs}
			svc, err := initService()
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}

			err = svc.Install()
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}
		},
	})

	rootCmd.AddCommand(&cobra.Command{
		Use:   "uninstall",
		Short: "Uninstall the perseus-agent service",
		Run:   serviceCmd(service.Service.Uninstall),
	})

	rootCmd.AddCommand(&cobra.Command{
		Use:   "start",
		Short: "Start the perseus-agent service",
		Run:   serviceCmd(service.Service.Start),

		PersistentPreRunE: requireValidConfig,
	})

	rootCmd.AddCommand(&cobra.Command{
		Use:   "stop",
		Short: "Stop the perseus-agent service",
		Run:   serviceCmd(service.Service.Stop),
	})

	rootCmd.AddCommand(&cobra.Command{
		Use:   "status",
		Short: "Get the status of the perseus-agent service",

		Run: func(*cobra.Command, []string) {
			svc, err := initService()
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}

			status, err := svc.Status()
			if err != nil {
				fmt.Fprintln(os.Stderr, err)
				os.Exit(1)
			}

			switch status {
			case service.StatusRunning:
				fmt.Println("perseus-agent is running")

			case service.StatusStopped:
				fmt.Println("perseus-agent is stopped")

			default:
				fmt.Println("perseus-agent status is unknown")
			}
		},
	})

	rootCmd.AddCommand(&cobra.Command{
		Use:   "run",
		Short: "Run the perseus-agent service",
		Run:   serviceCmd(service.Service.Run),

		PersistentPreRunE: requireValidConfig,
	})

	return struct{}{}
}()
