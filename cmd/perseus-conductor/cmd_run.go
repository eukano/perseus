package main

import (
	"bufio"
	"bytes"
	"context"
	"crypto"
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/eukano/perseus/internal/sys"
	"gitlab.com/eukano/perseus/messaging"
	"gitlab.com/eukano/perseus/signing"
	"gitlab.com/eukano/perseus/trust"
)

func runRun(cmd *cobra.Command, args []string) {
	if len(args) < 1 {
		cmd.Help()
		return
	}
	task, args := args[0], args[1:]

	if len(config.Brokers) == 0 {
		fmt.Fprintf(os.Stderr, "No brokers have been configured\n")
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	msg, err := trust.SignMessage(config.MustGetOwnKey(), "solicitation", &messaging.Solicitation{
		WillConnect: true,
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to format solicitation: %v\n", err)
		return
	}

	ownCert := config.Trust.MustGetOwnCert()
	ownFp := ownCert.Public().Fingerprint()

	signers, err := enumerateRecords(signing.SignerRecord)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to scan signers: %v\n", err)
		return
	}

	agents, err := enumerateRecords(signing.AgentRecord)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to scan agents: %v\n", err)
		return
	}

	for _, agent := range agents {
		if agent.Signatures == nil {
			agent.Signatures = map[string]string{}
		}
		for signerFp, recordedSig := range agent.Signatures {
			var signer trust.Certificate
			if signerFp == ownFp {
				signer = ownCert
			} else {
				record, ok := signers[signerFp]
				if !ok {
					fmt.Fprintf(os.Stderr, "Cannot verify signature for signer with fingerprint %s\n", signerFp)
					delete(agent.Signatures, signerFp)
					continue
				}
				signer = record.Certificate
			}

			i := strings.Index(recordedSig, ":")
			if i < 0 {
				fmt.Fprintf(os.Stderr, "Not a signature: %q\n", recordedSig)
				delete(agent.Signatures, signerFp)
				continue
			}

			if signer.Public().Type() != recordedSig[:i] {
				fmt.Fprintf(os.Stderr, "Signature type does not match key\n")
				delete(agent.Signatures, signerFp)
				continue
			}

			signature, err := base64.RawStdEncoding.DecodeString(recordedSig[i+1:])
			if err != nil {
				fmt.Fprintf(os.Stderr, "Not a signature: %q: %v\n", recordedSig, err)
				delete(agent.Signatures, signerFp)
				continue
			}

			digest := sha256.Sum256(agent.Certificate.Cert().Raw)
			err = signer.Public().Verify(digest[:], signature, crypto.SHA256)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Invalid signature: %q: %v\n", recordedSig, err)
				delete(agent.Signatures, signerFp)
				continue
			}
		}
	}

	response := false
	messages := make(chan *trust.SignedMessage)
	wmsg := new(sync.WaitGroup)

	for i, b := range config.Brokers {
		var topic = "perseus-announce"
		var queue = "perseus-announce"
		if b.AgentTopic != "" {
			topic = b.AgentTopic
		}
		if b.ReplyQueue != "" {
			queue = b.ReplyQueue
		}

		ctx, cancel := context.WithTimeout(ctx, time.Second)
		defer cancel()

		conn, err := b.Implementation().Connect(ctx, false)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Broker #%d: failed to connect: %v\n", i+1, err)
			continue
		}

		reply := queue + "." + id.String()
		sub, err := conn.SubscribeToQueue(reply)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Broker #%d: failed to subscribe: %v\n", i+1, err)
			continue
		}
		defer sub.Close()

		err = conn.SendToTopic(topic, reply, msg)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Broker #%d: failed to send solicitation: %v\n", i+1, err)
			continue
		}

		wmsg.Add(1)
		go func() {
			defer wmsg.Done()
			for {
				msg, ok := sub.Receive()
				if !ok {
					return
				}

				response = true
				body := msg.Body()
				switch body.MessageType {
				case "announcement":
					messages <- body

				default:
					fmt.Fprintf(os.Stderr, "Unexpected message type: %q\n", body.MessageType)
				}
			}
		}()
	}

	go func() {
		wmsg.Wait()
		close(messages)
	}()

	wconn := new(sync.WaitGroup)
	for msg := range messages {
		var record *recordAndCert

		signed := msg.Fingerprint != "" && len(msg.Signature) > 0
		if signed {
			var ok bool
			record, ok = agents[msg.Fingerprint]
			if ok {
				err := msg.Verify(record.Certificate.Public())
				if err != nil {
					fmt.Fprintf(os.Stderr, "Failed to verify signature: %v\n", err)
					continue
				}
			}
		}

		var announcement = new(messaging.Announcement)
		err := json.Unmarshal(msg.MessageData, announcement)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to unmarshal response payload: %v\n", err)
			continue
		}

		wconn.Add(1)
		go func() {
			defer wconn.Done()

			conn, ok := agentConnect(record, announcement)
			if !ok {
				return
			}
			defer conn.Close()

			agentExecute(task, args, conn, record)
		}()
	}

	wconn.Wait()

	if !response {
		fmt.Fprintf(os.Stderr, "No agent responses received\n")
	}
}

func agentConnect(record *recordAndCert, announcement *messaging.Announcement) (net.Conn, bool) {
	if record == nil {
		fmt.Fprintf(os.Stderr, "[%s] Record not found\n", announcement.Name)
		return nil, false
	}

	subject := record.Certificate.Cert().Subject
	if announcement.Connect == "" {
		fmt.Fprintf(os.Stderr, "[CN=%s] Connections not accepted\n", subject.CommonName)
		return nil, false
	}

	conn, err := net.Dial("tcp", announcement.Connect)
	if err != nil {
		fmt.Fprintf(os.Stderr, "[CN=%s] Failed to connect: %v\n", subject.CommonName, err)
		return nil, false
	}

	ownCert := config.Trust.MustGetOwnCert()
	ownKey := config.Trust.MustGetOwnKey()

	roots := x509.NewCertPool()
	roots.AddCert(record.Certificate.Cert())

	sconn := tls.Client(conn, &tls.Config{
		ServerName: subject.CommonName,
		Certificates: []tls.Certificate{
			tls.Certificate{
				Certificate: [][]byte{ownCert.Cert().Raw},
				PrivateKey:  ownKey.Underlying(),
			},
		},
		RootCAs: roots,
	})
	err = sconn.Handshake()
	if err != nil {
		fmt.Fprintf(os.Stderr, "[CN=%s] Failed to secure connection: %v\n", subject.CommonName, err)
		conn.Close()
		return nil, false
	}
	conn = sconn

	return conn, true
}

func agentExecute(task string, args []string, conn net.Conn, record *recordAndCert) {
	subject := record.Certificate.Cert().Subject

	_, err := bytes.NewBufferString(task + "\000").WriteTo(conn)
	if err != nil {
		fmt.Fprintf(os.Stderr, "[CN=%s] :( %v\n", subject.CommonName, err)
		return
	}

	r := bufio.NewReader(conn)
	for {
		line, err := r.ReadString('\n')
		if len(line) > 0 {
			if line[len(line)-1] == '\n' {
				line = line[:len(line)-1]
			}
			fmt.Printf("[CN=%s] %s\n", subject.CommonName, line)
		}

		if err == io.EOF || sys.ErrIsRST(err) {
			return
		}

		if err != nil {
			fmt.Fprintf(os.Stderr, "[CN=%s] :( %v\n", subject.CommonName, err)
			return
		}
	}
}
