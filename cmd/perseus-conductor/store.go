package main

import (
	"fmt"
	"os"

	"gitlab.com/eukano/perseus/signing"
	"gitlab.com/eukano/perseus/trust"
)

type recordAndCert struct {
	*signing.Record
	Certificate trust.Certificate
}

func enumerateRecords(typ signing.RecordType) (map[string]*recordAndCert, error) {
	records, err := config.SignatureStores.Stores().Scan(typ)
	if err != nil {
		return nil, err
	}

	result := map[string]*recordAndCert{}
	for _, records := range records {
		for _, record := range records {
			if len(record.Certificate) == 0 {
				fmt.Fprintf(os.Stderr, "Invalid record %v\n", record.Fingerprint)
				continue
			}

			cert, err := trust.UnmarshalCertificate(record.Certificate)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Failed to parse certificate: %v\n", err)
				continue
			}

			result[record.Fingerprint] = &recordAndCert{record, cert}
		}
	}
	return result, nil
}
