package main

import (
	"errors"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/eukano/perseus/conductor"
	"gitlab.com/eukano/perseus/internal/tls_debug"
)

var config *conductor.Configuration
var requireConfig, validateConfig bool

func requireValidConfig(cmd *cobra.Command, args []string) error {
	requireConfig = true
	validateConfig = true
	return rootCmd.PersistentPreRunE(cmd, args)
}

var rootCmd = func() *cobra.Command {
	var configPath string

	cmd := &cobra.Command{
		Use:   "perseus-conductor",
		Short: "Perseus Conductor is the Perseus CMS conductor tool",
		Long:  "Perseus Conductor is the conductor tool for the Perseus infrastructure configuration management system. Documentation is available at https://perseus-cms.io.",
		Run:   func(cmd *cobra.Command, _ []string) { cmd.Help() },

		PersistentPreRunE: func(*cobra.Command, []string) error {
			var err error
			if configPath == "" {
				config, err = conductor.FindConfiguration()
			} else {
				config, err = conductor.LoadConfiguration(configPath)
			}
			if !requireConfig && errors.Is(err, os.ErrNotExist) {
				config = new(conductor.Configuration)
			} else if err != nil {
				return err
			}

			if !validateConfig {
				return nil
			}
			return config.Validate()
		},
	}

	cmd.PersistentFlags().StringVarP(&configPath, "config", "C", "", "Configuration file")

	tls_debug.AddFlags(cmd)
	return cmd
}()

var pingCmd = func() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "ping",
		Short: "Ping agents",
		Run:   func(*cobra.Command, []string) { runPing() },

		PersistentPreRunE: requireValidConfig,
	}
	rootCmd.AddCommand(cmd)

	return cmd
}()

var runCmd = func() *cobra.Command {
	cmd := &cobra.Command{
		Use:   "run TASK",
		Short: "Run a task on agents",
		Run:   func(cmd *cobra.Command, args []string) { runRun(cmd, args) },

		PersistentPreRunE: requireValidConfig,
	}
	rootCmd.AddCommand(cmd)

	return cmd
}()
