package main

import (
	"context"
	"crypto"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"os"
	"strings"
	"sync"
	"time"

	"gitlab.com/eukano/perseus/messaging"
	"gitlab.com/eukano/perseus/signing"
	"gitlab.com/eukano/perseus/trust"
)

func runPing() {
	if len(config.Brokers) == 0 {
		fmt.Fprintf(os.Stderr, "No brokers have been configured\n")
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	msg, err := trust.SignMessage(config.MustGetOwnKey(), "solicitation", &messaging.Solicitation{
		WantMachineInfo: true,
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to format solicitation: %v\n", err)
		return
	}

	ownCert := config.Trust.MustGetOwnCert()
	ownFp := ownCert.Public().Fingerprint()

	signers, err := enumerateRecords(signing.SignerRecord)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to scan signers: %v\n", err)
		return
	}

	agents, err := enumerateRecords(signing.AgentRecord)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to scan agents: %v\n", err)
		return
	}

	for _, agent := range agents {
		if agent.Signatures == nil {
			agent.Signatures = map[string]string{}
		}
		for signerFp, recordedSig := range agent.Signatures {
			var signer trust.Certificate
			if signerFp == ownFp {
				signer = ownCert
			} else {
				record, ok := signers[signerFp]
				if !ok {
					fmt.Fprintf(os.Stderr, "Cannot verify signature for signer with fingerprint %s\n", signerFp)
					delete(agent.Signatures, signerFp)
					continue
				}
				signer = record.Certificate
			}

			i := strings.Index(recordedSig, ":")
			if i < 0 {
				fmt.Fprintf(os.Stderr, "Not a signature: %q\n", recordedSig)
				delete(agent.Signatures, signerFp)
				continue
			}

			if signer.Public().Type() != recordedSig[:i] {
				fmt.Fprintf(os.Stderr, "Signature type does not match key\n")
				delete(agent.Signatures, signerFp)
				continue
			}

			signature, err := base64.RawStdEncoding.DecodeString(recordedSig[i+1:])
			if err != nil {
				fmt.Fprintf(os.Stderr, "Not a signature: %q: %v\n", recordedSig, err)
				delete(agent.Signatures, signerFp)
				continue
			}

			digest := sha256.Sum256(agent.Certificate.Cert().Raw)
			err = signer.Public().Verify(digest[:], signature, crypto.SHA256)
			if err != nil {
				fmt.Fprintf(os.Stderr, "Invalid signature: %q: %v\n", recordedSig, err)
				delete(agent.Signatures, signerFp)
				continue
			}
		}
	}

	response := false
	messages := make(chan *trust.SignedMessage)
	wg := new(sync.WaitGroup)

	for i, b := range config.Brokers {
		var topic = "perseus-announce"
		var queue = "perseus-announce"
		if b.AgentTopic != "" {
			topic = b.AgentTopic
		}
		if b.ReplyQueue != "" {
			queue = b.ReplyQueue
		}

		ctx, cancel := context.WithTimeout(ctx, time.Second)
		defer cancel()

		conn, err := b.Implementation().Connect(ctx, false)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Broker #%d: failed to connect: %v\n", i+1, err)
			continue
		}

		reply := queue + "." + id.String()
		sub, err := conn.SubscribeToQueue(reply)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Broker #%d: failed to subscribe: %v\n", i+1, err)
			continue
		}
		defer sub.Close()

		err = conn.SendToTopic(topic, reply, msg)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Broker #%d: failed to send solicitation: %v\n", i+1, err)
			continue
		}

		wg.Add(1)
		go func() {
			defer wg.Done()
			for {
				msg, ok := sub.Receive()
				if !ok {
					return
				}

				response = true
				body := msg.Body()
				switch body.MessageType {
				case "announcement":
					messages <- body

				default:
					fmt.Fprintf(os.Stderr, "Unexpected message type: %q\n", body.MessageType)
				}
			}
		}()
	}

	go func() {
		wg.Wait()
		close(messages)
	}()

	for msg := range messages {
		var record *recordAndCert

		signed := msg.Fingerprint != "" && len(msg.Signature) > 0
		if signed {
			var ok bool
			record, ok = agents[msg.Fingerprint]
			if ok {
				err := msg.Verify(record.Certificate.Public())
				if err != nil {
					fmt.Fprintf(os.Stderr, "Failed to verify signature: %v\n", err)
					continue
				}
			}
		}

		var announcement messaging.Announcement
		err := json.Unmarshal(msg.MessageData, &announcement)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Failed to unmarshal response payload: %v\n", err)
			continue
		}

		if record == nil {
			fmt.Printf("- Name: %s\n", announcement.Name)
		} else {
			fmt.Printf("- Subject: %s\n", record.Certificate.Cert().Subject)
		}

		if announcement.Machine != nil {
			fmt.Printf("  Target: %s, %s\n", announcement.Machine.OS, announcement.Machine.Arch)
		}

		moreSigs := false
		if !signed {
			fmt.Printf("  Status: message is unsigned\n")
		} else if record == nil {
			fmt.Printf("  Status: agent is unknown\n")
		} else if len(record.Signatures) == 0 {
			fmt.Printf("  Status: agent record has no signatures\n")
		} else if _, ok := record.Signatures[ownFp]; ok {
			fmt.Printf("  Status: trusted\n")
			moreSigs = len(record.Signatures) > 1
		} else {
			fmt.Printf("  Status: not trusted\n")
			moreSigs = true
		}

		if moreSigs {
			fmt.Printf("  Other Signatories:\n")
			for fp := range record.Signatures {
				if fp == ownFp {
					continue
				}

				signer, ok := signers[fp]
				if !ok {
					fmt.Printf("  - Unknown (%s)\n", fp)
				} else {
					fmt.Printf("  - %s\n", signer.Certificate.Cert().Subject)
				}
			}
		}
	}

	if !response {
		fmt.Fprintf(os.Stderr, "No agent responses received\n")
	}
}
