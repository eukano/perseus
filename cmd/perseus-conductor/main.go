package main

import (
	"github.com/google/uuid"
)

var id = uuid.Must(uuid.NewRandom())

func main() {
	rootCmd.Execute()
}
