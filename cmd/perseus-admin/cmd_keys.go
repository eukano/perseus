package main

import (
	"context"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/eukano/perseus/messaging"
	"gitlab.com/eukano/perseus/signing"
	"gitlab.com/eukano/perseus/trust"
)

func runKeys(cmd *cobra.Command, fingerprint, generate, upload, listTrusted, listUnknown bool) {
	var didSomething bool
	var self = config.Trust.SelfStore()

	key, haveKey := config.Trust.GetOwnKey()
	cert, haveCert := config.Trust.GetOwnCert()

	if generate {
		didSomething = true
		if haveKey || haveCert {
			fmt.Fprintf(os.Stderr, "To regenerate the key and certificate, please delete them from %s first\n", self.Path())
		} else {
			err := config.Trust.EnsureOwnCertificate(config.Subject.Get())
			if err != nil {
				fmt.Fprintf(os.Stderr, "Failed to create a certificate: %v\n", err)
				os.Exit(1)
			}

			haveKey = true
			fmt.Println("Generated key")
		}
	}

	if fingerprint {
		didSomething = true
		if !haveKey {
			fmt.Fprintf(os.Stderr, "--fingerprint/-f is invalid without a key\n")
		} else {
			fmt.Println(key.Public().Fingerprint())
		}
	}

	if upload {
		didSomething = true
		if !haveCert {
			fmt.Fprintf(os.Stderr, "--upload is invalid without a certificate\n")
		} else {
			digest := sha256.Sum256(cert.Cert().Raw)
			record := &signing.Record{
				Digest:      "SHA256:" + base64.RawStdEncoding.EncodeToString(digest[:]),
				Type:        signing.SignerRecord,
				Name:        cert.Cert().Subject.String(),
				Fingerprint: cert.Public().Fingerprint(),
				Certificate: cert.Cert().Raw,
			}

			for name, store := range config.SignatureStores.Stores() {
				err := store.Put(record)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Failed to upload to store %q: %v\n", name, err)
				} else {
					fmt.Printf("Uploaded to store %q\n", name)
				}
			}
		}

	}

	if listTrusted {
		didSomething = true
		if len(config.SignatureStores) == 0 {
			fmt.Fprintf(os.Stderr, "No signature stores have been configured\n")
		} else if records, err := config.SignatureStores.Stores().Scan(signing.AgentRecord, "name", "fingerprint"); err != nil {
			fmt.Fprintf(os.Stderr, "Failed to scan stores: %v\n", err)
		} else {
			for name, records := range records {
				if len(records) == 0 {
					fmt.Fprintf(os.Stderr, "Trust store %q has no agent signatures\n", name)
					continue
				}

				fmt.Printf("Trust store %q\n", name)
				for _, record := range records {
					fmt.Printf("- %s as %s\n", record.Fingerprint, record.Name)
				}
			}
		}
	}

	if listUnknown {
		didSomething = true
		if !haveKey {
			fmt.Fprintf(os.Stderr, "--unknown/-n is invalid without a key\n")
		} else if len(config.Brokers) == 0 {
			fmt.Fprintf(os.Stderr, "At least one broker must be specified for --unknown/-n\n")
		} else {
			ctx, cancel := context.WithCancel(context.Background())
			defer cancel()

			msg, err := trust.SignMessage(key, "invitation", &messaging.Invitation{
				Name: config.Subject.CommonOrHostName(),
			})
			if err != nil {
				fmt.Fprintf(os.Stderr, "Failed to format solicitation: %v\n", err)
				return
			}

			response := false
			certs := make(chan trust.Certificate)
			wg := new(sync.WaitGroup)

			for i, b := range config.Brokers {
				var topic = "perseus-announce"
				var queue = "perseus-announce"
				if b.AgentTopic != "" {
					topic = b.AgentTopic
				}
				if b.ReplyQueue != "" {
					queue = b.ReplyQueue
				}

				ctx, cancel := context.WithTimeout(ctx, time.Second)
				defer cancel()

				conn, err := b.Implementation().Connect(ctx, false)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Broker #%d: failed to connect: %v\n", i+1, err)
					continue
				}

				reply := queue + "." + id.String()
				sub, err := conn.SubscribeToQueue(reply)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Broker #%d: failed to subscribe: %v\n", i+1, err)
					continue
				}
				defer sub.Close()

				err = conn.SendToTopic(topic, reply, msg)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Broker #%d: failed to send solicitation: %v\n", i+1, err)
					continue
				}

				wg.Add(1)
				go func() {
					defer wg.Done()
					for {
						msg, ok := sub.Receive()
						if !ok {
							return
						}
						response = true

						body := msg.Body()
						if body.MessageType != "invitation" {
							fmt.Fprintf(os.Stderr, "Unexpected response type: '%s'\n", body.MessageType)
							continue
						}

						var resp messaging.Invitation
						err := json.Unmarshal(body.MessageData, &resp)
						if err != nil {
							fmt.Fprintf(os.Stderr, "Failed to unmarshal response payload: %v\n", err)
							continue
						}

						if len(resp.Certificate) == 0 {
							fmt.Fprintf(os.Stderr, "Response has no key\n")
							continue
						}

						digest := sha256.Sum256(resp.Certificate)
						err = config.SignatureStores.Stores().Get(&signing.Record{
							Digest: "SHA256:" + base64.RawStdEncoding.EncodeToString(digest[:]),
						})
						if err == nil {
							continue
						} else if err != signing.ErrRecordNotFound {
							fmt.Fprintf(os.Stderr, "Failed to scan signature stores for fingerprint: %v\n", err)
							break
						}

						agentCert, err := trust.UnmarshalCertificate(resp.Certificate)
						if err != nil {
							fmt.Fprintf(os.Stderr, "Failed to unmarshal certificate: %v\n", err)
							continue
						}

						certs <- agentCert
					}
				}()
			}

			go func() {
				wg.Wait()
				close(certs)
			}()

			if cert, ok := <-certs; ok {
				fmt.Fprintln(os.Stderr, "Unknown agents")
				fmt.Printf("- %s as %s\n", cert.Public().Fingerprint(), cert.Cert().Subject)
			} else if response {
				fmt.Fprintf(os.Stderr, "All responding agents are known\n")
				return
			} else {
				fmt.Fprintf(os.Stderr, "No agent responses received\n")
				return
			}

			for cert := range certs {
				fmt.Printf("- %s as %s\n", cert.Public().Fingerprint(), cert.Cert().Subject)
			}
		}
	}

	if !didSomething {
		cmd.Usage()
	}
}
