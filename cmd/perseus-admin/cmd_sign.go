package main

import (
	"crypto"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/eukano/perseus/signing"
)

func runSign(storeName, name, osName, arch, file string, build bool) {
	if file == "" && !build {
		fmt.Fprintf(os.Stderr, "Either --build or --file is required\n")
		os.Exit(1)
	} else if file != "" && build {
		fmt.Fprintf(os.Stderr, "--build and --file cannot be used together\n")
		os.Exit(1)
	}

	bits := strings.Split(name, "@")
	if len(bits) != 2 {
		fmt.Fprintf(os.Stderr, "Invalid task@version: '%s'\n", name)
		os.Exit(1)
	}
	name = bits[0]
	version := bits[1]

	store, ok := config.SignatureStores.Store(storeName)
	if !ok {
		fmt.Fprintf(os.Stderr, "No store exists named %q\n", storeName)
		return
	}

	key, ok := config.Trust.GetOwnKey()
	if !ok {
		fmt.Fprintf(os.Stderr, "Self key not found; please update configuration or generate a new key\n")
		os.Exit(1)
	}

	if build {
		dir, err := ioutil.TempDir("", "perseus-build")
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}
		defer os.RemoveAll(dir)

		fmt.Println("Building...")

		cmd := exec.Command("go", "build", "-trimpath", "-ldflags=-s -w -buildid=", "-o", "task.exe", name)
		cmd.Env = append(os.Environ(), "GOOS="+osName, "GOARCH="+arch, "GO111MODULE=on")
		cmd.Dir = dir
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr

		err = cmd.Run()
		if err != nil {
			fmt.Fprintln(os.Stderr, err)
			os.Exit(1)
		}

		file = filepath.Join(dir, "task.exe")
	}

	raw, err := ioutil.ReadFile(file)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
		os.Exit(1)
	}

	digest := sha256.Sum256(raw)
	signature, err := key.Sign(rand.Reader, digest[:], crypto.SHA256)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to sign binary: %v\n", err)
		os.Exit(1)
	}

	record := &signing.Record{
		Digest:     "SHA256:" + base64.RawStdEncoding.EncodeToString(digest[:]),
		Signatures: map[string]string{},
	}
	err = store.Get(record)
	if err == nil && (record.Type != signing.TaskRecord || record.Name != name || record.Version != version || record.OS != osName || record.Arch != arch) {
		fmt.Fprintf(os.Stderr, "Invalid record: overwriting type, name, version, os, arch\n")
	}
	if err == nil || err == signing.ErrRecordNotFound {
		record.Type = signing.TaskRecord
		record.Name = name
		record.Version = version
		record.OS = osName
		record.Arch = arch
	} else if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to query signature store: %v\n", err)
		os.Exit(1)
	}

	record.Signatures[key.Public().Fingerprint()] = key.Type() + ":" + base64.RawStdEncoding.EncodeToString(signature)

	err = store.Put(record)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to push to signature store: %v\n", err)
		os.Exit(1)
	}
}
