package main

import (
	"errors"
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/eukano/perseus/admin"
	"gitlab.com/eukano/perseus/internal/tls_debug"
)

var config *admin.Configuration
var requireConfig, validateConfig bool

func requireValidConfig(cmd *cobra.Command, args []string) error {
	requireConfig = true
	validateConfig = true
	return rootCmd.PersistentPreRunE(cmd, args)
}

var rootCmd = func() *cobra.Command {
	var configPath string

	cmd := &cobra.Command{
		Use:   "perseus-admin",
		Short: "Perseus Admin is the Perseus CMS administration tool",
		Long:  "Perseus Admin is the administration tool for the Perseus infrastructure configuration management system. Documentation is available at https://perseus-cms.io.",
		Run:   func(cmd *cobra.Command, _ []string) { cmd.Help() },

		PersistentPreRunE: func(*cobra.Command, []string) error {
			var err error
			if configPath == "" {
				config, err = admin.FindConfiguration()
			} else {
				config, err = admin.LoadConfiguration(configPath)
			}
			if !requireConfig && errors.Is(err, os.ErrNotExist) {
				config = new(admin.Configuration)
			} else if err != nil {
				return err
			}

			if !validateConfig {
				return nil
			}
			return config.Validate()
		},
	}

	cmd.PersistentFlags().StringVarP(&configPath, "config", "C", "", "Configuration file")

	tls_debug.AddFlags(cmd)
	return cmd
}()

var signCmd = func() *cobra.Command {
	var store, name, os, arch, file string
	var build bool

	cmd := &cobra.Command{
		Use:   "sign",
		Short: "Sign tasks",
		Run:   func(*cobra.Command, []string) { runSign(store, name, os, arch, file, build) },

		PersistentPreRunE: requireValidConfig,
	}
	rootCmd.AddCommand(cmd)

	cmd.Flags().StringVarP(&store, "store", "s", "all", "The signature store to upload the signature to")
	cmd.Flags().StringVarP(&name, "name", "n", "", "The name@version of the task to sign")
	cmd.Flags().StringVarP(&os, "os", "o", "", "The OS the binary was built for")
	cmd.Flags().StringVarP(&arch, "arch", "a", "", "The architecture the binary was built for")
	cmd.Flags().StringVarP(&file, "file", "f", "", "The binary file")
	cmd.Flags().BoolVarP(&build, "build", "b", false, "Build the binary")

	cmd.MarkFlagRequired("store")
	cmd.MarkFlagRequired("name")
	cmd.MarkFlagRequired("os")
	cmd.MarkFlagRequired("arch")

	return cmd
}()

var keysCmd = func() *cobra.Command {
	var fingerprint, generate, upload, listTrusted, listUnknown bool

	cmd := &cobra.Command{
		Use:   "keys",
		Short: "Key tasks",

		Run: func(cmd *cobra.Command, _ []string) {
			runKeys(cmd, fingerprint, generate, upload, listTrusted, listUnknown)
		},

		PersistentPreRunE: requireValidConfig,
	}
	rootCmd.AddCommand(cmd)

	cmd.Flags().SortFlags = false
	cmd.Flags().BoolVarP(&fingerprint, "fingerprint", "f", false, "Display admin key fingerprint")
	cmd.Flags().BoolVar(&generate, "generate", false, "Generate a new admin key and self-signed admin certificate")
	cmd.Flags().BoolVar(&upload, "upload", false, "Upload the admin key and certificate to all signature stores")
	cmd.Flags().BoolVarP(&listTrusted, "trusted", "t", false, "List trusted agents")
	cmd.Flags().BoolVarP(&listUnknown, "unknown", "n", false, "List new and unknown agents")

	return cmd
}()

var inviteCmd = func() interface{} {
	var store, fingerprint string

	cmd := &cobra.Command{
		Use:   "invite",
		Short: "Invite agents",
		Run:   func(cmd *cobra.Command, args []string) { runInvite(store, fingerprint) },

		PersistentPreRunE: requireValidConfig,
	}
	rootCmd.AddCommand(cmd)

	cmd.Flags().SortFlags = false
	cmd.Flags().StringVarP(&store, "store", "s", "", "Signature store to add agent to")
	cmd.Flags().StringVarP(&fingerprint, "fingerprint", "f", "", "Fingerprint of the agent to invite")
	cmd.MarkFlagRequired("store")
	cmd.MarkFlagRequired("fingerprint")

	return struct{}{}
}()
