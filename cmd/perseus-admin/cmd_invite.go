package main

import (
	"bufio"
	"context"
	"crypto"
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"strings"
	"sync"
	"sync/atomic"
	"time"
	"unsafe"

	"gitlab.com/eukano/perseus/messaging"
	"gitlab.com/eukano/perseus/signing"
	"gitlab.com/eukano/perseus/trust"
)

func runInvite(storeName, fingerprint string) {
	var self = config.Trust.SelfStore()

	store, ok := config.SignatureStores.Store(storeName)
	if !ok {
		fmt.Fprintf(os.Stderr, "No store exists named %q\n", storeName)
		return
	}

	key, ok := config.Trust.GetOwnKey()
	if !ok {
		fmt.Fprintf(os.Stderr, "To invite agents, either generate a new key or add one to %s\n", self.Path())
		return
	}

	cert, ok := config.Trust.GetOwnCert()
	if !ok {
		fmt.Fprintf(os.Stderr, "To invite agents, either generate a new cert or add one to %s\n", self.Path())
		return
	}

	certBytes, err := trust.MarshalCertificate(cert)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to marshal certificate: %v\n", err)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	msg, err := trust.SignMessage(key, "invitation", &messaging.Invitation{
		Name:        config.Subject.CommonOrHostName(),
		For:         fingerprint,
		Certificate: certBytes,
	})
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to format solicitation: %v\n", err)
		return
	}

	type agentData struct {
		cert trust.Certificate
	}

	var agent *agentData

	wg := new(sync.WaitGroup)
	for i, b := range config.Brokers {
		var topic = "perseus-announce"
		var queue = "perseus-announce"
		if b.AgentTopic != "" {
			topic = b.AgentTopic
		}
		if b.ReplyQueue != "" {
			queue = b.ReplyQueue
		}

		ctx, cancel := context.WithTimeout(ctx, time.Second)
		defer cancel()

		conn, err := b.Implementation().Connect(ctx, false)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Broker #%d: failed to connect: %v\n", i+1, err)
			continue
		}

		reply := queue + "." + id.String()
		sub, err := conn.SubscribeToQueue(reply)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Broker #%d: failed to subscribe: %v\n", i+1, err)
			continue
		}
		defer sub.Close()

		err = conn.SendToTopic(topic, reply, msg)
		if err != nil {
			fmt.Fprintf(os.Stderr, "Broker #%d: failed to send solicitation: %v\n", i+1, err)
			continue
		}

		wg.Add(1)
		go func() {
			defer wg.Done()
			for {
				msg, ok := sub.Receive()
				if !ok {
					return
				}

				body := msg.Body()
				if body.MessageType != "invitation" {
					fmt.Fprintf(os.Stderr, "Unexpected response type: '%s'\n", body.MessageType)
					continue
				}

				if len(body.Signature) == 0 {
					fmt.Fprintf(os.Stderr, "Ignoring response with missing signature\n")
					continue
				}

				if body.Fingerprint == "" {
					fmt.Fprintf(os.Stderr, "Ignoring response with missing fingerprint\n")
					continue
				}

				var resp messaging.Invitation
				err := json.Unmarshal(body.MessageData, &resp)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Failed to unmarshal response payload: %v\n", err)
					continue
				}

				if len(resp.Certificate) == 0 {
					fmt.Fprintf(os.Stderr, "Response has no key\n")
					continue
				}

				cert, err := trust.UnmarshalCertificate(resp.Certificate)
				if err != nil {
					fmt.Fprintf(os.Stderr, "Failed to unmarshal certificate: %v\n", err)
					continue
				}

				if err := body.Verify(cert.Public()); err != nil {
					fmt.Fprintf(os.Stderr, "Message signature invalid: %v\n", err)
					continue
				}

				if cert.Public().Fingerprint() != fingerprint {
					continue
				}

				atomic.StorePointer((*unsafe.Pointer)(unsafe.Pointer(&agent)), unsafe.Pointer(&agentData{cert}))
			}
		}()
	}

	wg.Wait()

	if agent == nil {
		fmt.Fprintf(os.Stderr, "No response\n")
		return
	}

	agentSubject := agent.cert.Cert().Subject
	reader := bufio.NewReader(os.Stdin)
	for {
		fmt.Printf("Accept %v for %s (yes/no)? ", agentSubject, fingerprint)
		s, err := reader.ReadString('\n')
		if err != nil {
			log.Fatal(err)
		}

		s = strings.TrimSpace(s)
		if strings.EqualFold(s, "yes") {
			break
		}
		if strings.EqualFold(s, "no") {
			return
		}
	}

	digest := sha256.Sum256(agent.cert.Cert().Raw)
	signature, err := key.Sign(rand.Reader, digest[:], crypto.SHA256)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to sign agent key: %v\n", err)
		return
	}

	record := &signing.Record{
		Digest:     "SHA256:" + base64.RawStdEncoding.EncodeToString(digest[:]),
		Signatures: map[string]string{},
	}
	err = store.Get(record)
	if err == nil && (record.Type != signing.AgentRecord || record.Name != agentSubject.String()) {
		fmt.Fprintf(os.Stderr, "Invalid record: overwriting type, name\n")
	}
	if err == nil || err == signing.ErrRecordNotFound {
		record.Type = signing.AgentRecord
		record.Name = agentSubject.String()
		record.Fingerprint = agent.cert.Public().Fingerprint()
		record.Certificate = agent.cert.Cert().Raw
	} else if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to query signature store: %v\n", err)
		return
	}

	record.Signatures[key.Public().Fingerprint()] = key.Type() + ":" + base64.RawStdEncoding.EncodeToString(signature)

	err = store.Put(record)
	if err != nil {
		fmt.Fprintf(os.Stderr, "Failed to push to signature store: %v\n", err)
		return
	}

	fmt.Println("Added key to trust store")
}
