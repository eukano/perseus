package main

import (
	"gitlab.com/eukano/perseus/builder/backend/gitlab"
	"gitlab.com/eukano/perseus/builder/frontend/aws"
)

func main() {
	aws.Lambda(gitlab.CI)
}
