module gitlab.com/eukano/perseus

go 1.12

require (
	github.com/aws/aws-lambda-go v1.13.1
	github.com/aws/aws-sdk-go v1.23.13
	github.com/go-stomp/stomp v2.0.3+incompatible
	github.com/google/uuid v1.1.1
	github.com/kardianos/service v1.0.0
	github.com/pelletier/go-toml v1.4.0
	github.com/spf13/cobra v0.0.5
	golang.org/x/crypto v0.0.0-20190829043050-9756ffdc2472
	golang.org/x/sys v0.0.0-20190412213103-97732733099d
	golang.org/x/xerrors v0.0.0-20190717185122-a985d3407aa7
)

replace github.com/pelletier/go-toml v1.4.0 => github.com/pelletier/go-toml v0.0.0-20190827035702-3ded2e09ee1b
