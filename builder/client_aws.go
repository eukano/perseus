package builder

import (
	"net/http"
	"runtime"
)

type AWSAPIRequestBuilder struct {
	url, apiKey string
}

func (a *AWSAPIRequestBuilder) CreateRequest(name, version string) (*http.Request, error) {
	req, err := http.NewRequest(http.MethodGet, a.url, nil)
	if err != nil {
		return nil, err
	}

	q := req.URL.Query()
	q.Add("task", name)
	q.Add("version", version)
	q.Add("os", runtime.GOOS)
	q.Add("arch", runtime.GOARCH)
	req.URL.RawQuery = q.Encode()
	req.Header.Add("x-api-key", a.apiKey)

	return req, nil
}
