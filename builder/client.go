package builder

import (
	"strings"

	"golang.org/x/xerrors"
)

type Client interface {
	RetrieveTask(name, version string) (*Response, error)
}

type ClientKind string

const (
	DefaultClientKind       ClientKind = ""
	AWSAPIGatewayClientKind ClientKind = "aws-api-gateway"
)

type ClientConfiguration struct {
	implementation Client

	Name    string     `toml:"name,omitempty"`
	Kind    ClientKind `toml:"kind,omitempty"`
	Sources []string   `toml:"sources,omitempty"`

	AWS *AWSClientConfiguration `toml:"aws,omitempty"`
}

type ClientConfigurations []*ClientConfiguration

type AWSClientConfiguration struct {
	URL    string `toml:"url,omitempty"`
	APIKey string `toml:"api-key,omitempty"`
}

func (c *ClientConfiguration) Implementation() Client { return c.implementation }

func (c *ClientConfiguration) Validate() error {
	switch c.Kind {
	case DefaultClientKind:
		return xerrors.Errorf("[builders] builder kind must be specified")

	case AWSAPIGatewayClientKind:
		if err := c.AWS.Validate(); err != nil {
			return err
		}
		c.implementation = &HTTPClient{
			name: c.Name,
			reqb: &AWSAPIRequestBuilder{
				url:    c.AWS.URL,
				apiKey: c.AWS.APIKey,
			},
		}
		return nil

	default:
		return xerrors.Errorf("[builders] '%s' is not a supported builder kind", c.Kind)
	}
}

func (c *AWSClientConfiguration) Validate() error {
	if c == nil {
		return xerrors.Errorf("[builders] aws-api-gateway builder requires additional configuration in the [builders.aws] section")
	}

	if c.URL == "" {
		return xerrors.Errorf("[builders.aws] url is required")
	}

	if c.APIKey == "" {
		return xerrors.Errorf("[builders.aws] api-key is required")
	}

	return nil
}

func (c ClientConfigurations) ForTask(name string) (Client, bool) {
	if len(c) == 0 {
		return nil, false
	}

	for _, b := range c {
		for _, s := range b.Sources {
			if strings.HasPrefix(name, s+"/") && len(name) > len(s)+1 {
				return b.Implementation(), true
			}
		}
	}

	return nil, false
}
