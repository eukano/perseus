package aws

import (
	"encoding/base64"
	"encoding/json"
	"os"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/kms"
	"gitlab.com/eukano/perseus/builder"
)

func init() {
	kmsc := kms.New(session.New())
	crypttext, err := base64.StdEncoding.DecodeString(os.Getenv("GITLAB_TOKEN"))
	if err != nil {
		panic(err)
	}

	response, err := kmsc.Decrypt(&kms.DecryptInput{
		CiphertextBlob: crypttext,
	})
	if err != nil {
		panic(err)
	}

	os.Setenv("GITLAB_TOKEN", string(response.Plaintext[:]))
}

func Lambda(backend builder.Backend) {
	lambda.Start(func(gwrq events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
		if gwrq.HTTPMethod != "GET" {
			return events.APIGatewayProxyResponse{
				StatusCode: 405,
				Body:       "{Status: 0, Messages: ['Invalid method']}",
			}, nil
		}

		var request = builder.Request{}
		var response = builder.Response{Status: builder.Success}

		if !parseRequest(&gwrq, &request, &response) {
			response.Status = builder.Invalid
			return formatResponse(&response)
		}

		response, err := backend(request)
		if err != nil {
			return events.APIGatewayProxyResponse{
				StatusCode: 500,
			}, err
		}
		return formatResponse(&response)
	})
}

func parseRequest(gwrq *events.APIGatewayProxyRequest, request *builder.Request, response *builder.Response) bool {
	var allok = true
	var ok bool

	request.OS, ok = gwrq.QueryStringParameters["os"]
	if !ok {
		allok = false
		response.Messages = append(response.Messages, "Missing 'os' parameter")
	}

	request.Arch, ok = gwrq.QueryStringParameters["arch"]
	if !ok {
		allok = false
		response.Messages = append(response.Messages, "Missing 'arch' parameter")
	}

	request.Task, ok = gwrq.QueryStringParameters["task"]
	if !ok {
		allok = false
		response.Messages = append(response.Messages, "Missing 'task' parameter")
	}

	request.Version, ok = gwrq.QueryStringParameters["version"]
	if !ok {
		allok = false
		response.Messages = append(response.Messages, "Missing 'version' parameter")
	}

	return allok
}

func formatResponse(response *builder.Response) (events.APIGatewayProxyResponse, error) {
	var gwrs events.APIGatewayProxyResponse

	body, err := json.Marshal(response)
	if err != nil {
		return events.APIGatewayProxyResponse{
			StatusCode: 500,
		}, err
	}
	gwrs.Body = string(body)

	switch response.Status {
	case builder.Success, builder.Pending:
		gwrs.StatusCode = 200
	case builder.Invalid:
		gwrs.StatusCode = 400
	default:
		gwrs.StatusCode = 500
	}

	return gwrs, nil
}
