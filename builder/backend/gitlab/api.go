package gitlab

import (
	"bufio"
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httputil"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"golang.org/x/xerrors"
)

var dynamoDB = dynamodb.New(session.Must(session.NewSession()))
var cachingClient = &http.Client{Transport: &cachingTransport{dynamoDB}}

type cachingTransport struct {
	db *dynamodb.DynamoDB
}

func (t *cachingTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	if req.Method != http.MethodGet {
		return http.DefaultTransport.RoundTrip(req)
	}

	cached := t.get(req)
	if cached != nil {
		return cached, nil
	}

	resp, err := http.DefaultTransport.RoundTrip(req)
	if err != nil {
		return resp, err
	}

	t.set(req, resp)
	return resp, nil
}

func (t *cachingTransport) key(req *http.Request) string {
	url := *req.URL
	q := url.Query()
	q.Del("private_token")
	url.RawQuery = q.Encode()
	return "request:" + url.String()
}

func (t *cachingTransport) get(req *http.Request) *http.Response {
	result, err := t.db.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String("perseus-cache"),
		Key:       map[string]*dynamodb.AttributeValue{"key": {S: aws.String(t.key(req))}},
	})
	if result == nil || result.Item == nil || err != nil {
		return nil
	}
	av, ok := result.Item["value"]
	if !ok || av == nil || len(av.B) == 0 {
		return nil
	}

	resp, err := http.ReadResponse(bufio.NewReader(bytes.NewBuffer(av.B)), req)
	if err != nil {
		log.Printf("Error while retrieving response %v: %v", req.URL, err)
		return nil
	}

	date, err := time.Parse(time.RFC1123, resp.Header.Get("date"))
	if err != nil {
		log.Printf("Cached response for %v has invalid date: %v", req.URL, err)
		return nil
	}

	if time.Since(date) > 120*time.Hour {
		return nil
	}
	return resp
}

func (t *cachingTransport) set(req *http.Request, resp *http.Response) {
	if resp.Header.Get("date") == "" {
		return
	}

	raw, err := httputil.DumpResponse(resp, true)
	if err != nil {
		log.Printf("Error while caching response %v: %v", req.URL, err)
		return
	}

	av, err := dynamodbattribute.MarshalMap(map[string]interface{}{
		"key":   t.key(req),
		"ttl":   time.Now().Add(120 * time.Hour).Unix(),
		"value": raw,
	})
	if err == nil {
		t.db.PutItem(&dynamodb.PutItemInput{
			TableName: aws.String("perseus-cache"),
			Item:      av,
		})
	}
}

func client(cache bool) *http.Client {
	if cache {
		return cachingClient
	}
	return http.DefaultClient
}

func ProcessResponse(url string, resp *http.Response, v interface{}) error {
	if resp.StatusCode >= 400 {
		logResponse(url, resp.Body)
		return xerrors.Errorf("GET '%s' failed with %d %s", url, resp.StatusCode, resp.Status)
	}

	if resp.Header.Get("Content-Type") != "application/json" {
		logResponse(url, resp.Body)
		return xerrors.Errorf("GET '%s' failed: cannot process '%s' content", url, resp.Header.Get("Content-Type"))
	}

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return xerrors.Errorf("GET '%s': failed to read body: %v", url, err)
	}

	err = json.Unmarshal(b, v)
	if err != nil {
		log.Printf("GET '%s' response: %s", url, b)
		return xerrors.Errorf("GET '%s': failed to unmarshal response: %w", url, err)
	}

	return nil
}

func logResponse(url string, body io.Reader) {
	b, err := ioutil.ReadAll(body)
	if err != nil {
		log.Printf("GET '%s': failed to read body: %v", url, err)
	}
	if len(b) > 0 {
		log.Printf("GET '%s' response: %s", url, b)
	}
}

func Get(url string, cache bool, response interface{}) error {
	resp, err := client(cache).Get("https://gitlab.com/api/v4" + url)
	if err != nil {
		return xerrors.Errorf("GET '%s' failed: %w", url, err)
	}
	defer resp.Body.Close()

	return ProcessResponse(url, resp, response)
}

func Post(url, contentType string, body io.Reader, response interface{}) error {
	resp, err := http.Post("https://gitlab.com/api/v4"+url, contentType, body)
	if err != nil {
		return xerrors.Errorf("GET '%s' failed: %w", url, err)
	}
	defer resp.Body.Close()

	return ProcessResponse(url, resp, response)
}

func GetRaw(url string, cache bool) ([]byte, error) {
	resp, err := client(cache).Get("https://gitlab.com/api/v4" + url)
	if err != nil {
		return nil, xerrors.Errorf("GET '%s' failed: %w", url, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		logResponse(url, resp.Body)
		return nil, xerrors.Errorf("GET '%s' failed with %d %s", url, resp.StatusCode, resp.Status)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, xerrors.Errorf("GET '%s': failed to read body: %w", url, err)
	}
	return body, nil
}

func GoGet(url, token string, cache bool) (string, error) {
	resp, err := client(cache).Get(fmt.Sprintf("https://%s?go-get=1&private_token=%s", url, token))
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	dec := xml.NewDecoder(resp.Body)
	dec.Strict = false

	for {
		token, err := dec.RawToken()
		if err != nil {
			return "", err
		}

		switch token := token.(type) {
		case xml.EndElement:
			if strings.EqualFold(token.Name.Local, "head") {
				return "", nil
			}

		case xml.StartElement:
			if strings.EqualFold(token.Name.Local, "body") {
				return "", nil
			}

			if !strings.EqualFold(token.Name.Local, "meta") {
				continue
			}

			if getAttribute(token.Attr, "name") != "go-import" {
				continue
			}

			fields := strings.Fields(getAttribute(token.Attr, "content"))
			if len(fields) != 3 {
				continue
			}

			return fields[0], nil
		}
	}
}

func getAttribute(attrs []xml.Attr, name string) string {
	for _, attr := range attrs {
		if strings.EqualFold(attr.Name.Local, name) {
			return attr.Value
		}
	}
	return ""
}

type BuildStateValue string

const (
	Pending  BuildStateValue = "pending"
	Running  BuildStateValue = "running"
	Success  BuildStateValue = "success"
	Failed   BuildStateValue = "failed"
	Canceled BuildStateValue = "canceled"
	Skipped  BuildStateValue = "skipped"
)

type Pipeline struct {
	ID     int             `json:"id"`
	Status BuildStateValue `json:"status"`
	WebURL string          `json:"web_url"`
}

type PipelineVariable struct {
	VariableType string `json:"variable_type"`
	Key          string `json:"key"`
	Value        string `json:"value"`
}

type Job struct {
	ID     int             `json:"id"`
	Status BuildStateValue `json:"status"`
	Name   string          `json:"name"`
}
