package gitlab_test

import (
	"os"
	"testing"

	"gitlab.com/eukano/perseus/builder"
	"gitlab.com/eukano/perseus/builder/backend/gitlab"
)

func TestCI(t *testing.T) {
	os.Setenv("GITLAB_TOKEN", GitlabToken)

	resp, err := gitlab.CI(builder.Request{
		Task:    "gitlab.com/eukano/perseus-example/task/hello",
		Version: "master",
		OS:      "windows",
		Arch:    "amd64",
	})
	if err != nil {
		panic(err)
	}

	if resp.Status != builder.Success {
		t.Fatalf("%v != %v", resp.Status, builder.Success)
	}
}
