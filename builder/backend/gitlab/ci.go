package gitlab

import (
	"errors"
	"fmt"
	"log"
	"net/url"
	"os"
	"strings"
	"sync"
	"time"

	"github.com/aws/aws-sdk-go/aws/awserr"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"

	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"gitlab.com/eukano/perseus/builder"
	"golang.org/x/xerrors"
)

func CI(request builder.Request) (builder.Response, error) {
	token := os.Getenv("GITLAB_TOKEN")
	if token == "" {
		return builder.Response{}, errors.New("GITLAB_TOKEN is not set")
	}

	turl, err := url.Parse("https://" + request.Task)
	if err != nil {
		return builder.Response{
			Status: builder.Invalid,
			Messages: []string{
				fmt.Sprintf("Task must be a URL. Failed to parse: %v.", err),
			},
		}, nil
	}
	if turl.Host != "gitlab.com" {
		return builder.Response{
			Status: builder.Invalid,
			Messages: []string{
				fmt.Sprintf("Unsupported GitLab host: %s.", turl.Host),
				"Supported hosts are: gitlab.com",
			},
		}, nil
	}

	project, err := GoGet(request.Task, token, true)
	if err != nil {
		log.Printf("Failed to go-get '%s': %v", request.Task, err)
		return builder.Response{
			Status: builder.Invalid,
			Messages: []string{
				fmt.Sprintf("Failed to retrieve project path for '%s' (using ?go-get=1)", request.Task),
			},
		}, nil
	}
	if !strings.HasPrefix(project, "gitlab.com/") {
		return builder.Response{}, xerrors.Errorf("'%s' does not start with 'gitlab.com/'", project)
	}

	var task string
	if strings.HasPrefix(request.Task, project+"/") {
		task = request.Task[len(project)+1:]
	}
	project = project[len("gitlab.com/"):]

	if task == "" {
		return builder.Response{
			Status: builder.Invalid,
			Messages: []string{
				fmt.Sprintf("Server %s identified '%s' as the project for task '%s'.", turl.Host, project, request.Task),
				fmt.Sprintf("'%s' is an invalid task for project '%s'. Valid task names take the form PROJECT/TASK.", request.Task, project),
			},
		}, nil
	}

	pls, err := GetPipelines(project, task, request.Version, request.OS, request.Arch, token)
	if err != nil {
		return builder.Response{}, err
	}

	for _, pl := range pls {
		if pl.Status != Success {
			continue
		}

		exe, err := GetRaw(fmt.Sprintf("/projects/%s/jobs/%d/artifacts/task.exe?private_token=%s", url.PathEscape(project), pl.JobID, token), true)
		if err != nil {
			log.Printf("Failed to get task.exe from JID %d: %v", pl.JobID, err)
			continue
		}

		return builder.Response{
			Status: builder.Success,
			Binary: exe,
		}, nil
	}

	var pending, running *PerseusPipeline
	for _, pl := range pls {
		if pl.Status == Running {
			running = pl
			break
		}
		if pl.Status == Pending {
			pending = pl
			break
		}
	}

	if running != nil || pending != nil {
		var verb = "running"
		if running == nil {
			verb = "pending"
			running = pending
		}

		return builder.Response{
			Status: builder.Pending,
			Messages: []string{
				fmt.Sprintf("A build is %s. Please wait for it to complete. Status is available at %s", verb, running.PipelineWebURL),
			},
		}, nil
	}

	av, err := dynamodbattribute.MarshalMap(map[string]interface{}{
		"key": fmt.Sprintf("%s@%s:%s/%s", request.Task, request.Version, request.OS, request.Arch),
		"ttl": time.Now().Add(5 * time.Minute).Unix(),
	})
	if err != nil {
		return builder.Response{}, err
	}
	_, err = dynamoDB.PutItem(&dynamodb.PutItemInput{
		TableName:                aws.String("perseus-cache"),
		Item:                     av,
		ConditionExpression:      aws.String("attribute_not_exists(#k)"),
		ExpressionAttributeNames: map[string]*string{"#k": aws.String("key")},
	})
	if azerr, ok := err.(awserr.Error); ok && azerr.Code() == dynamodb.ErrCodeConditionalCheckFailedException {
		return builder.Response{
			Status: builder.Pending,
			Messages: []string{
				fmt.Sprintf("A build is has been launched by another request. Please wait for it to complete."),
			},
		}, nil
	}
	if err != nil {
		log.Println(err)
	}

	createUrl := fmt.Sprintf("/projects/%s/pipeline?private_token=%s&ref=%s", url.PathEscape(project), token, request.Version)
	createUrl += "&variables[][key]=GOOS&variables[][value]=" + url.QueryEscape(request.OS)
	createUrl += "&variables[][key]=GOARCH&variables[][value]=" + url.QueryEscape(request.Arch)
	createUrl += "&variables[][key]=TASK&variables[][value]=" + url.QueryEscape(task)

	var pl Pipeline
	err = Post(createUrl, "", nil, &pl)

	delete(av, "ttl")
	dynamoDB.DeleteItem(&dynamodb.DeleteItemInput{
		TableName: aws.String("perseus-cache"),
		Key:       av,
	})

	if err != nil {
		log.Printf("Failed to create pipeline: %v", err)
		return builder.Response{}, err
	}

	return builder.Response{
		Status: builder.Pending,
		Messages: []string{
			fmt.Sprintf("A build is has been launched. Please wait for it to complete. Status is available at %s", pl.WebURL),
		},
	}, nil
}

type PerseusPipeline struct {
	Status         BuildStateValue
	JobID          int
	PipelineWebURL string
}

func GetPipelines(project, task, version, os, arch, token string) ([]*PerseusPipeline, error) {
	var allpl []*Pipeline
	var pls = []*PerseusPipeline{}
	var vars []*PipelineVariable
	var jobs []*Job

	err := Get(fmt.Sprintf("/projects/%s/pipelines?private_token=%s&ref=%s", url.PathEscape(project), token, version), false, &allpl)
	if err != nil {
		return nil, err
	}

	plch := make(chan *PerseusPipeline)
	wg := new(sync.WaitGroup)
	wg.Add(len(allpl))

	go func() { wg.Wait(); close(plch) }()

	for _, pl := range allpl {
		pl := pl
		go func() {
			defer wg.Done()

			var got_os, got_arch, got_task bool
			var job *Job

			if !(pl.Status == Success || pl.Status == Running || pl.Status == Pending) {
				return
			}

			err = Get(fmt.Sprintf("/projects/%s/pipelines/%d/variables?private_token=%s", url.PathEscape(project), pl.ID, token), true, &vars)
			if err != nil {
				log.Printf("Error while getting pipeline variables: %v", err)
				return
			}

			for _, v := range vars {
				if v.VariableType != "env_var" {
					return
				}

				switch v.Key {
				case "GOOS":
					if v.Value == os {
						got_os = true
					}

				case "GOARCH":
					if v.Value == arch {
						got_arch = true
					}

				case "TASK":
					if v.Value == task {
						got_task = true
					}
				}
			}

			err = Get(fmt.Sprintf("/projects/%s/pipelines/%d/jobs?private_token=%s", url.PathEscape(project), pl.ID, token), false, &jobs)
			if err != nil {
				log.Printf("Error while getting pipeline job: %v", err)
				return
			}
			for _, job = range jobs {
				if job.Name == "perseus" {
					break
				}
			}
			if job == nil || job.Name != "perseus" {
				return
			}
			if !(job.Status == Success || job.Status == Running || job.Status == Pending) {
				return
			}

			if got_os && got_arch && got_task {
				plch <- &PerseusPipeline{
					Status:         job.Status,
					JobID:          job.ID,
					PipelineWebURL: pl.WebURL,
				}
			}
		}()
	}

	for pl := range plch {
		pls = append(pls, pl)
	}
	return pls, nil
}
