package builder

type Frontend func(Backend)

type Backend func(Request) (Response, error)

type Request struct {
	Task, Version, OS, Arch string
}

type Status int

const (
	Invalid Status = iota
	Success
	Pending
	Failed
)

type Response struct {
	Status   Status
	Messages []string
	Binary   []byte
}
