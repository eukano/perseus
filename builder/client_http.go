package builder

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"golang.org/x/xerrors"
)

var (
	ErrCouldNotCreateRequest     = fmt.Errorf("builder/http-client: could not create request")
	ErrFailedToExecuteRequest    = fmt.Errorf("builder/http-client: failed to execute request")
	ErrRequestFailed             = fmt.Errorf("builder/http-client: request failed")
	ErrInvalidContentType        = fmt.Errorf("builder/http-client: invalid content type")
	ErrFailedToReadRequestBody   = fmt.Errorf("builder/http-client: failed to read request body")
	ErrFailedToUnmarshalResponse = fmt.Errorf("builder/http-client: failed to unmarshal response")
)

type HTTPClient struct {
	name string
	reqb HTTPRequestBuilder
}

type HTTPRequestBuilder interface {
	CreateRequest(name, version string) (*http.Request, error)
}

func (c *HTTPClient) RetrieveTask(name, version string) (*Response, error) {
	req, err := c.reqb.CreateRequest(name, version)
	if err != nil {
		return nil, xerrors.Errorf("[%s] %w: %v", c.name, ErrCouldNotCreateRequest, err)
	}

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, xerrors.Errorf("[%s] %w for task %q: %v", c.name, ErrFailedToExecuteRequest, name, err)
	}
	defer resp.Body.Close()

	if resp.StatusCode >= 400 {
		return nil, xerrors.Errorf("[%s] %w for task %q: %d %s", c.name, ErrRequestFailed, name, resp.StatusCode, resp.Status)
	}

	contentType := resp.Header.Get("Content-Type")
	if contentType != "application/json" {
		return nil, xerrors.Errorf("[%s] %w for task %q: %q", c.name, ErrInvalidContentType, name, contentType)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, xerrors.Errorf("[%s] %w for task %q: %v", c.name, ErrFailedToReadRequestBody, name, err)
	}

	var r Response
	err = json.Unmarshal(body, &r)
	if err != nil {
		return nil, xerrors.Errorf("[%s] %w for task %q: %v", c.name, ErrFailedToUnmarshalResponse, name, err)
	}

	return &r, nil
}

func logResponse(resp *http.Response, log func(string)) {
	fmt.Printf("Request ID: %s\n", resp.Header.Get("x-amzn-RequestId"))
	fmt.Printf("Gateway ID: %s\n", resp.Header.Get("x-amz-apigw-id"))
	fmt.Printf("Trace ID: %s\n", resp.Header.Get("x-Amzn-Trace-Id"))
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log(fmt.Sprintf("failed to read body: %v", err))
	}
	if len(b) > 0 {
		log(string(b))
	}
}
