package trust

import (
	"crypto/x509/pkix"
	"os"

	"golang.org/x/xerrors"
)

type SubjectConfiguration struct {
	hostname string

	Country            []string `toml:"country,omitempty"`
	Organization       []string `toml:"organization,omitempty"`
	OrganizationalUnit []string `toml:"organizational-unit,omitempty"`
	Locality           []string `toml:"locality,omitempty"`
	Province           []string `toml:"province,omitempty"`
	StreetAddress      []string `toml:"street-address,omitempty"`
	PostalCode         []string `toml:"postal-code,omitempty"`
	CommonName         string   `toml:"common-name,omitempty"`
	// SerialNumber       string   `toml:"serial-number,omitempty"`
}

func (c *SubjectConfiguration) CommonOrHostName() string {
	if c.CommonName == "" {
		return c.hostname
	}
	return c.CommonName
}

func (c *SubjectConfiguration) Validate(useHostname bool) error {
	if c.CommonName == "" {
		if !useHostname {
			return xerrors.Errorf("[subject] common-name is required")
		}

		var err error
		c.hostname, err = os.Hostname()
		if err != nil {
			return xerrors.Errorf("[subject] common-name unspecified, failed to get hostname: %w", err)
		}
	}

	return nil
}

func (c *SubjectConfiguration) Get() *pkix.Name {
	subject := &pkix.Name{
		Country:            c.Country,
		Organization:       c.Organization,
		OrganizationalUnit: c.OrganizationalUnit,
		Locality:           c.Locality,
		Province:           c.Province,
		StreetAddress:      c.StreetAddress,
		PostalCode:         c.PostalCode,
		CommonName:         c.CommonName,
		// SerialNumber:       c.SerialNumber,
	}

	if subject.CommonName != "" {
		return subject
	}

	if c.hostname == "" {
		panic("cannot determine common name for subject")
	}

	subject.CommonName = c.hostname
	return subject
}

func SubjectEquals(a, b *pkix.Name) bool {
	return subjectStringSliceEquals(a.Country, b.Country) &&
		subjectStringSliceEquals(a.Organization, b.Organization) &&
		subjectStringSliceEquals(a.OrganizationalUnit, b.OrganizationalUnit) &&
		subjectStringSliceEquals(a.Locality, b.Locality) &&
		subjectStringSliceEquals(a.Province, b.Province) &&
		subjectStringSliceEquals(a.StreetAddress, b.StreetAddress) &&
		subjectStringSliceEquals(a.PostalCode, b.PostalCode) &&
		// a.SerialNumber == b.SerialNumber &&
		a.CommonName == b.CommonName
}

func subjectStringSliceEquals(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}

	return true
}
