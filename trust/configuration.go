package trust

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"crypto/x509/pkix"
	"math/big"
	"time"

	"golang.org/x/xerrors"
)

type Configuration struct {
	self, trusted, pending, cached *Store

	Self    string `toml:"self,omitempty"`
	Trusted string `toml:"trusted,omitempty"`
	Pending string `toml:"pending,omitempty"`
	Cached  string `toml:"cached,omitempty"`
}

type ConfigOptions struct {
	RequireSelf    bool
	RequireTrusted bool
	RequirePending bool
	RequireCached  bool
	DefaultSelf    string
	DefaultTrusted string
	DefaultPending string
	DefaultCached  string
}

func (c *Configuration) SelfStore() *Store    { return c.self }
func (c *Configuration) TrustedStore() *Store { return c.trusted }
func (c *Configuration) PendingStore() *Store { return c.pending }
func (c *Configuration) CachedStore() *Store  { return c.cached }
func (c *Configuration) Stores() Stores       { return Stores{c.self, c.trusted, c.pending, c.cached} }

func (c *Configuration) Validate(subject *pkix.Name, opts *ConfigOptions) error {
	if opts == nil {
		opts = new(ConfigOptions)
	}

	stores := map[string]struct {
		path     string
		store    **Store
		required bool
		fallback string
	}{
		"self":    {c.Self, &c.self, opts.RequireSelf, opts.DefaultSelf},
		"trusted": {c.Trusted, &c.trusted, opts.RequireTrusted, opts.DefaultTrusted},
		"pending": {c.Pending, &c.pending, opts.RequirePending, opts.DefaultPending},
		"cached":  {c.Cached, &c.cached, opts.RequireCached, opts.DefaultCached},
	}

	for name, opts := range stores {
		if opts.path == "" {
			if opts.fallback != "" {
				opts.path = opts.fallback
			} else if opts.required {
				return xerrors.Errorf("[trust] %s is required", name)
			}
		}

		s, err := LoadStore(opts.path)
		if err != nil {
			return xerrors.Errorf("[trust] failed to load %s: %w", name, err)
		}

		*opts.store = s
	}

	if subject != nil && len(c.self.certificates) > 0 {
		if !SubjectEquals(&c.self.certificates[0].Cert().Subject, subject) {
			return xerrors.Errorf("[trust] own certificate subject %v does not match [subject] %v; please recreate the certificate or update [subject]", c.self.certificates[0].Cert().Subject, subject)
		}
	}

	return nil
}

func (c *Configuration) EnsureOwnCertificate(subject *pkix.Name) error {
	if len(c.self.certificates) > 0 {
		if subject != nil && !SubjectEquals(&c.self.certificates[0].Cert().Subject, subject) {
			return xerrors.Errorf("[trust] own certificate subject %v does not match [subject] %v; please recreate the certificate or update [subject]", c.self.certificates[0].Cert().Subject, subject)
		}
		return nil
	}

	if len(c.self.privateKeys) == 0 {
		key, err := rsa.GenerateKey(rand.Reader, 2048)
		if err != nil {
			return xerrors.Errorf("failed to generate key: %w", err)
		}

		priv, err := NewPrivateKey(key)
		if err != nil {
			return xerrors.Errorf("failed to generate key: %w", err)
		}

		c.self.AddPrivateKey(priv)
	}

	serialNumberLimit := new(big.Int).Lsh(big.NewInt(1), 128)
	serialNumber, err := rand.Int(rand.Reader, serialNumberLimit)
	if err != nil {
		return xerrors.Errorf("[trust] failed to create serial number for certificate: %w", err)
	}

	tmpl := &x509.Certificate{
		SerialNumber: serialNumber,
		Subject:      *subject,
		DNSNames:     []string{subject.CommonName},

		NotBefore: time.Now(),
		NotAfter:  time.Now().AddDate(5, 0, 0),

		KeyUsage:              x509.KeyUsageKeyEncipherment | x509.KeyUsageDigitalSignature,
		ExtKeyUsage:           []x509.ExtKeyUsage{x509.ExtKeyUsageServerAuth},
		BasicConstraintsValid: true,
	}
	priv := c.self.privateKeys[0]
	raw, err := x509.CreateCertificate(rand.Reader, tmpl, tmpl, priv.Public().Underlying(), priv.Underlying())
	if err != nil {
		return xerrors.Errorf("failed to generate certificate: %w", err)
	}

	xcert, err := x509.ParseCertificate(raw)
	if err != nil {
		panic(err) // if x509.CreateCertificate doesn't return a valid ASN.1 DER certificate... something is wrong
	}

	cert, err := NewCertificate(xcert)
	if err != nil {
		return xerrors.Errorf("failed to generate certificate: %w", err)
	}

	c.self.AddCertificate(cert)
	err = c.self.Save()
	if err != nil {
		return xerrors.Errorf("failed to save generated key: %w", err)
	}
	return nil
}

func (c *Configuration) GetOwnKey() (PrivateKey, bool) {
	if len(c.self.privateKeys) == 0 {
		return nil, false
	}
	return c.MustGetOwnKey(), true
}

func (c *Configuration) MustGetOwnKey() PrivateKey {
	return c.self.privateKeys[0]
}

func (c *Configuration) GetOwnCert() (Certificate, bool) {
	if len(c.self.certificates) == 0 {
		return nil, false
	}
	return c.MustGetOwnCert(), true
}

func (c *Configuration) MustGetOwnCert() Certificate {
	return c.self.certificates[0]
}
