package trust

import (
	"crypto"
	"crypto/ecdsa"
	"encoding/asn1"
	"errors"
	"io"
	"math/big"

	"golang.org/x/crypto/ssh"
)

var ErrECDSADecryption = errors.New("crypto/ecdsa: decryption error")
var ErrECDSAVerification = errors.New("crypto/ecdsa: verification error")

func (k *ecdsaPrivateKey) Underlying() crypto.PrivateKey { return k.ecdsa }
func (k *ecdsaPublicKey) Underlying() crypto.PublicKey   { return k.ecdsa }

type ecdsaPrivateKey struct {
	withHeaders
	ecdsa *ecdsa.PrivateKey
	pub   *ecdsaPublicKey
}

func (k *ecdsaPrivateKey) Type() string      { return "ECDSA" }
func (k *ecdsaPrivateKey) Public() PublicKey { return k.pub }

func (k *ecdsaPrivateKey) Sign(rand io.Reader, digest []byte, opts crypto.SignerOpts) ([]byte, error) {
	r, s, err := ecdsa.Sign(rand, k.ecdsa, digest)
	if err != nil {
		return nil, err
	}

	return asn1.Marshal(ecdsaSignature{r, s})
}

type ecdsaPublicKey struct {
	withHeaders
	ecdsa *ecdsa.PublicKey
	ssh   ssh.PublicKey
}

type ecdsaSignature struct {
	R, S *big.Int
}

func (k *ecdsaPublicKey) Type() string        { return "ECDSA" }
func (k *ecdsaPublicKey) Marshal() []byte     { return k.ssh.Marshal() }
func (k *ecdsaPublicKey) Fingerprint() string { return ssh.FingerprintSHA256(k.ssh) }

func (k *ecdsaPublicKey) Verify(digest, sig []byte, opts crypto.SignerOpts) error {
	var rs ecdsaSignature
	_, err := asn1.Unmarshal(sig, &rs)
	if err != nil {
		return ErrECDSADecryption
	}

	if !ecdsa.Verify(k.ecdsa, digest, rs.R, rs.S) {
		return ErrECDSAVerification
	}
	return nil
}
