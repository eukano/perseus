package trust

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/x509"
	"io"

	"golang.org/x/crypto/ssh"
	"golang.org/x/xerrors"
)

type PrivateKey interface {
	Underlying() crypto.PrivateKey
	Type() string
	Headers() Headers
	Sign(rand io.Reader, digest []byte, opts crypto.SignerOpts) (signature []byte, err error)
	Public() PublicKey
}

type PublicKey interface {
	Underlying() crypto.PublicKey
	Type() string
	Headers() Headers
	Verify(digest, sig []byte, opts crypto.SignerOpts) error
	Marshal() []byte
	Fingerprint() string
}

func NewPrivateKey(key interface{}) (PrivateKey, error) {
	switch key := key.(type) {
	case *rsa.PrivateKey:
		pub := &key.PublicKey
		ssh, err := ssh.NewPublicKey(pub)
		if err != nil {
			return nil, err
		}

		return &rsaPrivateKey{withHeaders{Headers{}}, key, &rsaPublicKey{withHeaders{nil}, pub, ssh}}, nil

	case *ecdsa.PrivateKey:
		pub := &key.PublicKey
		ssh, err := ssh.NewPublicKey(pub)
		if err != nil {
			return nil, err
		}

		return &ecdsaPrivateKey{withHeaders{Headers{}}, key, &ecdsaPublicKey{withHeaders{nil}, pub, ssh}}, nil
	}

	return nil, xerrors.Errorf("unsupported key type: expected RSA or ECDSA, got %T", key)
}

func NewPublicKey(key interface{}) (PublicKey, error) {
	switch key := key.(type) {
	case *rsa.PublicKey:
		ssh, err := ssh.NewPublicKey(key)
		if err != nil {
			return nil, err
		}

		return &rsaPublicKey{withHeaders{Headers{}}, key, ssh}, nil

	case *ecdsa.PublicKey:
		ssh, err := ssh.NewPublicKey(key)
		if err != nil {
			return nil, err
		}

		return &ecdsaPublicKey{withHeaders{Headers{}}, key, ssh}, nil
	}

	return nil, xerrors.Errorf("unsupported key type: expected RSA or ECDSA, got %T", key)
}

func UnmarshalPrivateKey(data []byte) (PrivateKey, error) {
	x, err := x509.ParsePKCS8PrivateKey(data)
	if err != nil {
		return nil, err
	}

	key, err := NewPrivateKey(x)
	if err != nil {
		return nil, err
	}

	return key, nil
}

func MarshalPrivateKey(key PrivateKey) ([]byte, error) {
	var priv interface{}
	switch key := key.(type) {
	case *rsaPrivateKey:
		priv = key.rsa
	case *ecdsaPrivateKey:
		priv = key.ecdsa
	default:
		return nil, xerrors.Errorf("unsupported key type: %T", key)
	}

	return x509.MarshalPKCS8PrivateKey(priv)
}

func UnmarshalPublicKey(data []byte) (PublicKey, error) {
	x, err := x509.ParsePKIXPublicKey(data)
	if err != nil {
		return nil, err
	}

	key, err := NewPublicKey(x)
	if err != nil {
		return nil, err
	}

	return key, nil
}

func MarshalPublicKey(key PublicKey) ([]byte, error) {
	var pub interface{}
	switch key := key.(type) {
	case *rsaPublicKey:
		pub = key.rsa
	case *ecdsaPublicKey:
		pub = key.ecdsa
	default:
		return nil, xerrors.Errorf("unsupported key type: %T", key)
	}

	return x509.MarshalPKIXPublicKey(pub)
}
