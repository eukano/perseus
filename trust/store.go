package trust

import (
	"bytes"
	"encoding/pem"
	"io/ioutil"
	"os"
	"sync"

	"golang.org/x/xerrors"
)

func LoadStore(path string) (*Store, error) {
	s := &Store{
		path:         path,
		privateKeys:  []PrivateKey{},
		publicKeys:   []PublicKey{},
		certificates: []Certificate{},
	}

	raw, err := ioutil.ReadFile(path)
	if os.IsNotExist(err) {
		return s, nil
	}
	if err != nil {
		return nil, err
	}

	for len(raw) > 0 {
		pblock, rest := pem.Decode(raw)
		if pblock == nil {
			if len(rest) == 0 {
				break
			}

			i := bytes.IndexByte(rest, '\n')
			if i < 0 {
				return nil, xerrors.Errorf("unexpected data while decoding PEM blocks: '%s'", rest)
			}
			return nil, xerrors.Errorf("unexpected data while decoding PEM blocks: '%s'", rest[:i])
		}
		raw = rest

		switch pblock.Type {
		case "PRIVATE KEY":
			key, err := UnmarshalPrivateKey(pblock.Bytes)
			if err != nil {
				return nil, err
			}

			key.Headers().Add(pblock.Headers)
			s.privateKeys = append(s.privateKeys, key)

		case "PUBLIC KEY":
			key, err := UnmarshalPublicKey(pblock.Bytes)
			if err != nil {
				return nil, err
			}

			key.Headers().Add(pblock.Headers)
			s.publicKeys = append(s.publicKeys, key)

		case "CERTIFICATE":
			cert, err := UnmarshalCertificate(pblock.Bytes)
			if err != nil {
				return nil, err
			}

			cert.Headers().Add(pblock.Headers)
			s.certificates = append(s.certificates, cert)

		default:
			return nil, xerrors.Errorf("unsupported PEM block type: '%s'", pblock.Type)
		}
	}

	return s, nil
}

func SaveStore(path string, s *Store) error {
	raw := []byte{}

	for _, k := range s.privateKeys {
		b, err := MarshalPrivateKey(k)
		if err != nil {
			return err
		}

		raw = append(raw, pem.EncodeToMemory(&pem.Block{
			Type:    "PRIVATE KEY",
			Bytes:   b,
			Headers: k.Headers(),
		})...)
	}

	for _, k := range s.publicKeys {
		b, err := MarshalPublicKey(k)
		if err != nil {
			return err
		}

		raw = append(raw, pem.EncodeToMemory(&pem.Block{
			Type:    "PUBLIC KEY",
			Bytes:   b,
			Headers: k.Headers(),
		})...)
	}

	for _, c := range s.certificates {
		raw = append(raw, pem.EncodeToMemory(&pem.Block{
			Type:    "CERTIFICATE",
			Bytes:   c.Cert().Raw,
			Headers: c.Headers(),
		})...)
	}

	return ioutil.WriteFile(path, raw, 0600)
}

const (
	forLookup = iota
	forAdd
	forDelete
)

type Store struct {
	mu         sync.RWMutex
	path       string
	certLookup map[string]Certificate
	keyLookup  map[string]PublicKey

	privateKeys  []PrivateKey
	publicKeys   []PublicKey
	certificates []Certificate
}

func (s *Store) Path() string { return s.path }
func (s *Store) Save() error  { return SaveStore(s.path, s) }

func (s *Store) PrivateKeys() []PrivateKey   { return s.privateKeys }
func (s *Store) PublicKeys() []PublicKey     { return s.publicKeys }
func (s *Store) Certificates() []Certificate { return s.certificates }

func (s *Store) acquireLookup(mode int) (gotExclusive bool) {
	if mode != forAdd {
		s.mu.RLock()
		if mode == forDelete && s.certLookup == nil || s.certLookup != nil {
			return false
		}
		s.mu.RUnlock()
	}

	s.mu.Lock()
	if s.certLookup != nil {
		return true
	}

	s.certLookup = map[string]Certificate{}
	s.keyLookup = map[string]PublicKey{}

	for _, cert := range s.certificates {
		s.certLookup[cert.Public().Fingerprint()] = cert
		s.keyLookup[cert.Public().Fingerprint()] = cert.Public()
	}
	for _, key := range s.publicKeys {
		s.keyLookup[key.Fingerprint()] = key
	}
	return true
}

func (s *Store) AddPrivateKey(key PrivateKey) {
	s.privateKeys = append(s.privateKeys, key)
}

func (s *Store) AddPublicKey(key PublicKey) {
	s.publicKeys = append(s.publicKeys, key)

	s.acquireLookup(forAdd)
	s.keyLookup[key.Fingerprint()] = key
	s.mu.Unlock()
}

func (s *Store) AddCertificate(key Certificate) {
	s.certificates = append(s.certificates, key)

	s.acquireLookup(forAdd)
	s.certLookup[key.Public().Fingerprint()] = key
	s.keyLookup[key.Public().Fingerprint()] = key.Public()
	s.mu.Unlock()
}

func (s *Store) DeletePrivateKey(key PrivateKey) bool {
	for i, k := range s.privateKeys {
		if k == key {
			s.privateKeys = append(s.privateKeys[:i], s.privateKeys[i+1:]...)
			return true
		}
	}
	return false
}

func (s *Store) DeletePublicKey(key PublicKey) bool {
	if !s.acquireLookup(forDelete) {
		s.mu.RUnlock()
	} else {
		delete(s.keyLookup, key.Fingerprint())
		s.mu.Unlock()
	}

	for i, k := range s.publicKeys {
		if k == key {
			s.publicKeys = append(s.publicKeys[:i], s.publicKeys[i+1:]...)
			return true
		}
	}
	return false
}

func (s *Store) DeleteCertificate(cert Certificate) bool {
	if !s.acquireLookup(forDelete) {
		s.mu.RUnlock()
	} else {
		delete(s.keyLookup, cert.Public().Fingerprint())
		delete(s.certLookup, cert.Public().Fingerprint())
		s.mu.Unlock()
	}

	for i, c := range s.certificates {
		if c == cert {
			s.certificates = append(s.certificates[:i], s.certificates[i+1:]...)
			return true
		}
	}
	return false
}

func (s *Store) Locate(fingerprint string) (Certificate, PublicKey, bool) {
	if !s.acquireLookup(forLookup) {
		cert, ok := s.certLookup[fingerprint]
		if ok {
			s.mu.RUnlock()
			return cert, cert.Public(), true
		}
		key, ok := s.keyLookup[fingerprint]
		if ok {
			s.mu.RUnlock()
			return nil, key, true
		}
		s.mu.RUnlock()
		s.mu.Lock()
	}

	cert, ok := s.certLookup[fingerprint]
	if ok {
		s.mu.Unlock()
		return cert, cert.Public(), true
	}
	key, ok := s.keyLookup[fingerprint]
	if ok {
		s.mu.Unlock()
		return nil, key, true
	}

	s.mu.Unlock()
	return nil, nil, false
}

type Stores []*Store

func (s Stores) Locate(fingerprint string) (Certificate, PublicKey, int) {
	for i, s := range s {
		cert, pub, ok := s.Locate(fingerprint)
		if ok {
			return cert, pub, i
		}
	}
	return nil, nil, -1
}
