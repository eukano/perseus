package trust

type Headers map[string]string

func (h Headers) Get(key string) (value string, ok bool) {
	value, ok = h[key]
	return
}

func (h Headers) Set(key, value string) {
	h[key] = value
}

func (h Headers) Add(i map[string]string) {
	for k, v := range i {
		h[k] = v
	}
}

type withHeaders struct {
	headers Headers
}

func (w *withHeaders) Headers() Headers { return w.headers }
