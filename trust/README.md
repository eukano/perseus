# Trust

## Expectations

  * Trust stores are PEM files (a file only containing PEM blocks)
  * Private keys are PKCS8 encoded
  * Public keys are PKIX encoded
  * Certificates are ASN.1 DER encoded