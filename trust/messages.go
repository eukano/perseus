package trust

import (
	"crypto"
	"crypto/rand"
	"crypto/sha256"
	"encoding/json"

	"golang.org/x/xerrors"
)

func SignMessage(key PrivateKey, typ string, body interface{}) (*SignedMessage, error) {
	raw, err := json.Marshal(body)
	if err != nil {
		return nil, err
	}

	msg := &SignedMessage{
		MessageType: typ,
		MessageData: raw,
	}

	err = msg.Sign(key)
	if err != nil {
		return nil, err
	}

	return msg, nil
}

type SignedMessage struct {
	Fingerprint string `json:"fingerprint,omitempty"`
	Signature   []byte `json:"signature,omitempty"`
	MessageType string `json:"message-type,omitempty"`
	MessageData []byte `json:"message-data,omitempty"`
}

func (s *SignedMessage) Marshal() ([]byte, error) {
	return json.Marshal(s)
}

func (s *SignedMessage) Unmarshal(data []byte) error {
	return json.Unmarshal(data, s)
}

func (s *SignedMessage) Sign(key PrivateKey) error {
	digest := sha256.Sum256(append([]byte(s.MessageType), s.MessageData...))
	sig, err := key.Sign(rand.Reader, digest[:], crypto.SHA256)
	if err != nil {
		return err
	}

	s.Signature = sig
	s.Fingerprint = key.Public().Fingerprint()
	return nil
}

func (s *SignedMessage) Verify(key PublicKey) error {
	if len(s.Signature) == 0 {
		return xerrors.Errorf("this message is not signed")
	}
	if s.Fingerprint != key.Fingerprint() {
		return xerrors.Errorf("a different key was used to sign this message")
	}

	digest := sha256.Sum256(append([]byte(s.MessageType), s.MessageData...))
	return key.Verify(digest[:], s.Signature, crypto.SHA256)
}
