package trust

import "crypto/x509"

func NewCertificate(cert *x509.Certificate) (Certificate, error) {
	pub, err := NewPublicKey(cert.PublicKey)
	if err != nil {
		return nil, err
	}

	return &certificate{withHeaders{Headers{}}, pub, cert}, nil
}

type Certificate interface {
	Headers() Headers
	Public() PublicKey
	Cert() *x509.Certificate
}

type certificate struct {
	withHeaders
	pub  PublicKey
	cert *x509.Certificate
}

func (c *certificate) Public() PublicKey       { return c.pub }
func (c *certificate) Cert() *x509.Certificate { return c.cert }

func UnmarshalCertificate(data []byte) (Certificate, error) {
	x, err := x509.ParseCertificate(data)
	if err != nil {
		return nil, err
	}

	cert, err := NewCertificate(x)
	if err != nil {
		return nil, err
	}

	return cert, nil
}

func MarshalCertificate(cert Certificate) ([]byte, error) {
	return cert.Cert().Raw, nil
}
