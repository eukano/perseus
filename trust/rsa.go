package trust

import (
	"crypto"
	"crypto/rsa"
	"io"

	"golang.org/x/crypto/ssh"
)

func (k *rsaPrivateKey) Underlying() crypto.PrivateKey { return k.rsa }
func (k *rsaPublicKey) Underlying() crypto.PublicKey   { return k.rsa }

type rsaPrivateKey struct {
	withHeaders
	rsa *rsa.PrivateKey
	pub *rsaPublicKey
}

func (k *rsaPrivateKey) Type() string      { return "RSA" }
func (k *rsaPrivateKey) Public() PublicKey { return k.pub }

func (k *rsaPrivateKey) Sign(rand io.Reader, digest []byte, opts crypto.SignerOpts) ([]byte, error) {
	return rsa.SignPKCS1v15(rand, k.rsa, opts.HashFunc(), digest)
}

type rsaPublicKey struct {
	withHeaders
	rsa *rsa.PublicKey
	ssh ssh.PublicKey
}

func (k *rsaPublicKey) Type() string        { return "RSA" }
func (k *rsaPublicKey) Marshal() []byte     { return k.ssh.Marshal() }
func (k *rsaPublicKey) Fingerprint() string { return ssh.FingerprintSHA256(k.ssh) }

func (k *rsaPublicKey) Verify(digest, sig []byte, opts crypto.SignerOpts) error {
	return rsa.VerifyPKCS1v15(k.rsa, opts.HashFunc(), digest, sig)
}
