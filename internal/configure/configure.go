package configure

import (
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
	"reflect"

	"github.com/pelletier/go-toml"
	"gitlab.com/eukano/perseus/trust"
	"golang.org/x/xerrors"
)

var fieldLookup = map[reflect.Type]int{}
var typeConfigFile = reflect.TypeOf(ConfigFile{})

func DefaultPath(name string) string {
	if defaultConfigDirectory == "" {
		return name
	}
	return filepath.Join(defaultConfigDirectory, name)
}

func Find(name string, config interface{}) error {
	if defaultConfigDirectory != "" {
		err := Load(filepath.Join(defaultConfigDirectory, name), config)
		if err == nil || !os.IsNotExist(err) {
			return err
		}
	}

	err := Load(filepath.Join(".", name), config)
	if err == nil || !os.IsNotExist(err) {
		return err
	}

	ex, err := os.Executable()
	if err != nil {
		return err
	}

	err = Load(filepath.Join(filepath.Dir(ex), name), config)
	if err == nil || !os.IsNotExist(err) {
		return err
	}

	return fmt.Errorf("could not find %s at any of the expected locations: %w", name, os.ErrNotExist)
}

func Load(path string, config interface{}) error {
	v := reflect.ValueOf(config)
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}

	typ := v.Type()
	field, ok := fieldLookup[typ]
	if !ok {
		if typ.Kind() != reflect.Struct {
			panic(xerrors.Errorf("expected struct or struct pointer, got %T", config))
		}

		for i := 0; i < typ.NumField(); i++ {
			f := typ.Field(i)
			if !f.Anonymous || f.PkgPath != "" {
				continue
			}
			typ := f.Type
			if typ.Kind() == reflect.Ptr {
				typ = typ.Elem()
			}
			if typ != typeConfigFile {
				continue
			}

			field = i
			ok = true
			break
		}

		if !ok {
			field = -1
			fieldLookup[typ] = -1
		}
	}

	if field < 0 {
		panic(xerrors.Errorf("type %T does not have an anonymous %T field", config, ConfigFile{}))
	}

	f, err := os.Open(path)
	if err != nil {
		return xerrors.Errorf("failed to load configuration from '%s': %w", path, err)
	}
	defer f.Close()

	err = toml.NewDecoder(f).Decode(config)
	if err != nil {
		return xerrors.Errorf("failed to parse configuration from '%s': %w", path, err)
	}

	file := ConfigFile{loadPath: path}
	file.absolutePath, file.absolutePathError = filepath.Abs(path)

	v = v.Field(field)
	if v.Kind() != reflect.Ptr {
		v.Set(reflect.ValueOf(file))
	} else if v.IsNil() {
		v.Set(reflect.ValueOf(&file))
	} else {
		v.Elem().Set(reflect.ValueOf(file))
	}
	return nil
}

func Save(path string, config interface{}) error {
	raw, err := toml.Marshal(config)
	if err != nil {
		return err
	}

	return ioutil.WriteFile(path, raw, 0600)
}

type ConfigFile struct {
	loadPath, absolutePath string
	absolutePathError      error
}

func (c *ConfigFile) LoadPath() string {
	return c.loadPath
}

func (c *ConfigFile) AbsolutePath() (string, error) {
	return c.absolutePath, c.absolutePathError
}

func (c *ConfigFile) TrustOptions() *trust.ConfigOptions {
	var dir = "."
	if abs, err := c.AbsolutePath(); err == nil && abs != "" {
		dir = filepath.Dir(abs)
	} else if ldp := c.LoadPath(); ldp != "" {
		dir = filepath.Dir(ldp)
	}

	return &trust.ConfigOptions{
		RequireSelf:    true,
		RequireTrusted: true,
		RequirePending: true,
		RequireCached:  true,
		DefaultSelf:    filepath.Join(dir, "secrets.pem"),
		DefaultTrusted: filepath.Join(dir, "trusted.pem"),
		DefaultPending: filepath.Join(dir, "pending.pem"),
		DefaultCached:  filepath.Join(dir, "cached.pem"),
	}
}
