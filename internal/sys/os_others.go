// +build !darwin,!linux,!windows

package sys

import (
	"errors"
	"os"
)

func CreateTempTask() (*os.File, error) {
	return nil, errors.New("unknown platform: cannot execute task")
}

func ErrIsRST(error) bool {
	return false
}
