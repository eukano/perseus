package sys

import (
	"errors"
	"io/ioutil"
	"net"
	"os"
	"syscall"

	"golang.org/x/sys/windows"
)

func CreateTempTask() (*os.File, error) {
	return ioutil.TempFile("", "perseus-task-*.exe")
}

func ErrIsRST(err error) bool {
	var nerr *net.OpError
	if errors.As(err, &nerr) {
		err = nerr.Err
	} else {
		return false
	}

	var serr *os.SyscallError
	if errors.As(err, &serr) {
		err = serr.Err
	} else {
		return false
	}

	var errno syscall.Errno
	if errors.As(err, &errno) {
		return errno == windows.WSAECONNRESET
	} else {
		return false
	}
}
