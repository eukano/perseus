package sys

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"syscall"

	"golang.org/x/sys/unix"
)

func CreateTempTask() (*os.File, error) {
	return ioutil.TempFile("", "perseus-task-*")
}

func ErrIsRST(err error) bool {
	var nerr *net.OpError
	if errors.As(err, &nerr) {
		err = nerr.Err
	} else {
		return false
	}

	var serr *os.SyscallError
	if errors.As(err, &serr) {
		err = serr.Err
	} else {
		return false
	}

	var errno syscall.Errno
	if errors.As(err, &errno) {
		fmt.Printf("%d\n", errno)
		return errno == unix.ECONNRESET
	} else {
		return false
	}
}
