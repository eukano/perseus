// +build !tls_debug

package tls_debug

import (
	"context"
	"crypto/tls"

	"github.com/spf13/cobra"
)

func AddFlags(cmd *cobra.Command) {}

func Configure(ctx context.Context, config *tls.Config) *tls.Config {
	return config
}
