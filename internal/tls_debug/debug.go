// +build tls_debug

package tls_debug

import (
	"context"
	"crypto/tls"
	"os"

	"github.com/spf13/cobra"
)

var tlsKeyLogPath string

func AddFlags(cmd *cobra.Command) {
	cmd.PersistentFlags().StringVar(&tlsKeyLogPath, "tls-key-log", "", "TLS key log file")
	cmd.Flag("tls-key-log").Hidden = true
}

func Configure(ctx context.Context, config *tls.Config) *tls.Config {
	if tlsKeyLogPath == "" {
		return config
	}

	if config == nil {
		config = new(tls.Config)
	}

	keylog, err := os.OpenFile(tlsKeyLogPath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	go func() { <-ctx.Done(); keylog.Close() }()

	config.KeyLogWriter = keylog
	return config
}
