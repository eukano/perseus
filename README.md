# Perseus CMS

**`Perseus CMS is under development and not ready for prime-time`**

The Perseus Configuration Management System aims to provide lean and robust
tooling that can be used to bootstrap infrastructure with minimal per-node setup
and minimal long term maintenance costs. To this end, Perseus CMS has a small
core feature set and avoids centralized systems. It does require some
centralization, but this can be provided by off-the-shelf products from cloud
infrastructure providers, moving the majority of the burden of maintenance to
the cloud provider, with minimal costs incurred by the user. Because of this
minimal feature set, Perseus CMS may be more appropriately labeled an
infrastructure or task automation system.

Your infrastructure management needs may be beyond what Perseus CMS is designed
for. If that is the case, consider using Perseus CMS as a tool to bootstrap a
more feature-rich CMS system such as SaltStack, Ansible, or Puppet.

## TODO

  * ***Documentation***
  * Scripts for setting up AWS resources
  * Include common tasks (built-in or provided)
  * [Full list](TODO.md)