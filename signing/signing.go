package signing

import "fmt"

var ErrRecordNotFound = fmt.Errorf("record not found")

type RecordType string

const (
	TaskRecord   RecordType = "task"
	AgentRecord  RecordType = "agent"
	SignerRecord RecordType = "signer"
)

type Store interface {
	Get(record *Record) error
	Put(record *Record) error
	Scan(RecordType, ...string) ([]*Record, error)
}

type Record struct {
	Digest      string            `dynamodbav:"digest,omitempty"`
	Type        RecordType        `dynamodbav:"type,omitempty"`
	Name        string            `dynamodbav:"name,omitempty"`
	Version     string            `dynamodbav:"version,omitempty"`
	OS          string            `dynamodbav:"os,omitempty"`
	Arch        string            `dynamodbav:"arch,omitempty"`
	Fingerprint string            `dynamodbav:"fingerprint,omitempty"`
	Certificate []byte            `dynamodbav:"certificate,omitempty"`
	Signatures  map[string]string `dynamodbav:"signatures,omitempty"`
}

type StoreSet map[string]Store

func (set StoreSet) Get(record *Record) error {
	for _, store := range set {
		err := store.Get(record)
		if err == nil {
			return nil
		}
		if err != ErrRecordNotFound {
			return err
		}
	}
	return ErrRecordNotFound
}

func (set StoreSet) Scan(typ RecordType, attrs ...string) (map[string][]*Record, error) {
	var groups = map[string][]*Record{}
	var err error

	for name, store := range set {
		groups[name], err = store.Scan(typ, attrs...)
		if err != nil {
			return nil, err
		}
	}

	return groups, nil
}
