package signing

import (
	"errors"
	"fmt"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

type DynamoDBSignatureStore struct {
	DB    *dynamodb.DynamoDB
	Table string
}

func (s *DynamoDBSignatureStore) Get(record *Record) error {
	if record == nil {
		return errors.New("invalid query: record must not be null")
	}

	key, err := dynamodbattribute.MarshalMap(&Record{Digest: record.Digest})
	if err != nil {
		return err
	}

	result, err := s.DB.GetItem(&dynamodb.GetItemInput{
		TableName: aws.String(s.Table),
		Key:       key,
	})
	if err != nil {
		return err
	}
	if result.Item == nil {
		return ErrRecordNotFound
	}

	err = dynamodbattribute.UnmarshalMap(result.Item, record)
	if err != nil {
		return err
	}

	return nil
}

func (s *DynamoDBSignatureStore) Put(record *Record) error {
	av, err := dynamodbattribute.MarshalMap(record)
	if err != nil {
		return err
	}

	_, err = s.DB.PutItem(&dynamodb.PutItemInput{
		TableName: aws.String(s.Table),
		Item:      av,
	})
	if err != nil {
		return err
	}

	return nil
}

func (s *DynamoDBSignatureStore) Scan(typ RecordType, attributes ...string) ([]*Record, error) {
	eav, err := dynamodbattribute.MarshalMap(map[string]string{":type": string(typ)})
	if err != nil {
		panic(err)
	}

	query := new(dynamodb.ScanInput)
	query.SetTableName(s.Table)
	query.SetFilterExpression("#type = :type")
	query.SetExpressionAttributeValues(eav)
	query.SetExpressionAttributeNames(map[string]*string{"#type": aws.String("type")})

	if len(attributes) > 0 {
		expr := ""
		for i, attr := range attributes {
			s := fmt.Sprintf("#%d", i)
			if i > 0 {
				expr += ","
			}
			expr += s
			query.ExpressionAttributeNames[s] = aws.String(attr)
		}
		query.SetProjectionExpression(expr)
	}

	result, err := s.DB.Scan(query)
	if err != nil {
		return nil, err
	}

	records := make([]*Record, len(result.Items))
	for i, item := range result.Items {
		records[i] = new(Record)
		err = dynamodbattribute.UnmarshalMap(item, records[i])
		if err != nil {
			return nil, err
		}
	}

	return records, nil
}
