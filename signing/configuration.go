package signing

import (
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"golang.org/x/xerrors"
)

type StoreKind string

const (
	DefaultStoreKind  StoreKind = ""
	DynamoDBStoreKind StoreKind = "dynamo-db"
)

type Configuration struct {
	implementation Store

	Kind    StoreKind `toml:"kind,omitempty"`
	Sources []string  `toml:"sources,omitempty"`

	DynamoDB *DynamoDBConfiguration `toml:"dynamo-db,omitempty"`
}

type DynamoDBConfiguration struct {
	Region string `toml:"region,omitempty"`
	Table  string `toml:"table,omitempty"`

	AccessKeyID     string `toml:"access-key-id,omitempty"`
	SecretAccessKey string `toml:"secret-access-key,omitempty"`
}

func (c *Configuration) Store() Store {
	return c.implementation
}

func (c *Configuration) Validate() error {
	switch c.Kind {
	case DefaultStoreKind:
		return xerrors.Errorf("[signature-store] store kind must be specified")

	case DynamoDBStoreKind:
		if c.DynamoDB == nil {
			return xerrors.Errorf("[signature-store] dynamo-db signature store requires additional configuration in the [signature-store.dynamo-db] section")
		}
		if impl, err := c.DynamoDB.Validate(); err != nil {
			return err
		} else {
			c.implementation = impl
		}
		return nil

	default:
		return xerrors.Errorf("[signature-store] '%s' is not a supported signature store kind", c.Kind)
	}
}

func (c *DynamoDBConfiguration) Validate() (*DynamoDBSignatureStore, error) {
	if c.Region == "" {
		return nil, xerrors.Errorf("[signature-store.dynamo-db] region must be specified")
	}
	if c.Table == "" {
		return nil, xerrors.Errorf("[signature-store.dynamo-db] table must be specified")
	}

	config := &aws.Config{
		Region: aws.String(c.Region),
	}
	if c.AccessKeyID != "" || c.SecretAccessKey != "" {
		config.Credentials = credentials.NewStaticCredentials(c.AccessKeyID, c.SecretAccessKey, os.Getenv("AWS_SESSION_TOKEN"))
	}
	session, err := session.NewSession(config)
	if err != nil {
		return nil, err
	}

	return &DynamoDBSignatureStore{
		DB:    dynamodb.New(session),
		Table: c.Table,
	}, nil
}

type ConfigurationSet map[string]*Configuration

func (c ConfigurationSet) Validate() error {
	for _, c := range c {
		if err := c.Validate(); err != nil {
			return err
		}
	}
	return nil
}

func (c ConfigurationSet) Store(name string) (Store, bool) {
	store, ok := c[name]
	if !ok {
		return nil, false
	}
	return store.implementation, true
}

func (c ConfigurationSet) Stores() StoreSet {
	set := StoreSet{}
	for name, c := range c {
		set[name] = c.implementation
	}
	return set
}
