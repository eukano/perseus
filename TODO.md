
  * Be more strict about semver?

  * Built-in tasks? System info, filtering, etc. Or just tell consumers to copy task shells into their project.
  * Verification of source (go mod sum)
  * Verification of binary (build id in binary)
  * Builder bits for things other than AWS Lambda + GitLab Runner
  * Integrate `go` into lambda, for one-stop building?
  * Handle CI failures
  * Agent caches tasks

  * Tasks can be built into agent
  * One EXE can have multiple tasks: `gitlab.com/org/tasks/process:task`
  * Provide core tasks, which can be built in
  * Provide common tasks, which can be built in
  * Provide a signing key, builder, and signing db for provided tasks
  * Allow clients to set up their own signing key/db/builder for provided tasks