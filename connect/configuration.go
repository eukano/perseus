package connect

import (
	"time"

	"golang.org/x/xerrors"
)

type Method string

const (
	DefaultMethod Method = ""
	None          Method = "none"
	ByName        Method = "by-name"
	Local         Method = "local"
	Remote        Method = "remote"
)

type Configuration struct {
	manager Manager

	Method    Method        `toml:"method,omitempty"`
	Name      string        `toml:"name,omitempty"`
	Port      int           `toml:"port,omitempty"`
	Interface string        `toml:"interface,omitempty"`
	Timeout   time.Duration `toml:"timeout,omitempty"`
}

func (c *Configuration) Manager() Manager { return c.manager }

func (c *Configuration) Validate() error {
	cm := &SimpleManager{
		network: "tcp",
		port:    c.Port,
		timeout: c.Timeout,
	}
	if cm.timeout == 0 {
		cm.timeout = 5 * time.Minute
	}

	switch c.Method {
	case DefaultMethod, Local:
		cm.announce = "127.0.0.1"
		cm.address = "127.0.0.1"
		c.manager = cm
		return nil

	case None:
		c.manager = nullManager{}
		return nil

	case ByName:
		if c.Name == "" {
			return xerrors.Errorf("[announce.connect] name must be specified when method is by-name")
		}

		cm.announce = c.Name
		c.manager = cm

		if c.Interface != "" {
			address, err := interfaceAddress(c.Interface)
			if err != nil {
				return err
			}
			cm.address = address
		}
		return nil

	case Remote:
		return xerrors.Errorf("[announce.connect] remote connections are not supported at this time")

	default:
		return xerrors.Errorf("[announce.connect] '%s' is not a supported connection method", c.Method)
	}
}
