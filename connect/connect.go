package connect

import (
	"fmt"
	"net"
	"sync"
	"time"

	"golang.org/x/xerrors"
)

type Manager interface {
	Listen(func(net.Conn, error)) (string, error)
}

type listener interface {
	net.Listener

	SetDeadline(time.Time) error
}

func interfaceAddress(name string) (string, error) {
	if name == "" {
		return "", nil
	}

	iface, err := net.InterfaceByName(name)
	if err != nil {
		return "", err
	}

	addrs, err := iface.Addrs()
	if err != nil {
		return "", err
	}

	if len(addrs) == 0 {
		return "", xerrors.Errorf("interface has no addresses")
	}

	return addrs[0].String(), nil
}

type nullManager struct{}

func (nullManager) Listen(func(net.Conn, error)) (string, error) {
	return "", nil
}

type SimpleManager struct {
	announce string
	network  string
	address  string
	port     int
	timeout  time.Duration

	mu        sync.RWMutex
	ln        listener
	advertize string
}

func (m *SimpleManager) Listen(handle func(net.Conn, error)) (advertize string, err error) {
	m.mu.RLock()
	if m.ln != nil {
		go m.accept(m.ln, handle)
		m.mu.RUnlock()
		return m.advertize, nil
	}
	m.mu.RUnlock()

	m.mu.Lock()
	defer m.mu.Unlock()
	if m.ln != nil {
		go m.accept(m.ln, handle)
		return m.advertize, nil
	}

	ln, err := net.Listen(m.network, fmt.Sprintf("%s:%d", m.address, m.port))
	if err != nil {
		return "", err
	}
	defer func() {
		if err != nil {
			ln.Close()
			m.ln = nil
			m.advertize = ""
		}
	}()

	var ok bool
	m.ln, ok = ln.(listener)
	if !ok {
		return "", xerrors.Errorf("cannot set a deadline on %T", ln)
	}
	defer func() {
		if err == nil {
			m.advertize = advertize
			go m.accept(m.ln, handle)
		}
	}()

	if m.port > 0 {
		return fmt.Sprintf("%s:%d", m.announce, m.port), nil
	}

	// get port from listener
	switch addr := ln.Addr().(type) {
	case *net.TCPAddr:
		return fmt.Sprintf("%s:%d", m.announce, addr.Port), nil

	case *net.UDPAddr:
		return fmt.Sprintf("%s:%d", m.announce, addr.Port), nil

	default:
		ln.Close()
		return "", xerrors.Errorf("expected tcp or udp socket; got %T", addr)
	}
}

func (m *SimpleManager) accept(ln listener, handle func(net.Conn, error)) {
	var conn net.Conn
	var err error
	defer func() { handle(conn, err) }()

	defer func() {
		if err != nil {
			m.mu.Lock()
			ln.Close()
			m.ln = nil
			m.advertize = ""
			m.mu.Unlock()
		}
	}()

	err = ln.SetDeadline(time.Now().Add(m.timeout))
	if err != nil {
		return
	}

	conn, err = ln.Accept()
	return
}
