package agent

import (
	"gitlab.com/eukano/perseus/builder"
	"gitlab.com/eukano/perseus/connect"
	"gitlab.com/eukano/perseus/internal/configure"
	"gitlab.com/eukano/perseus/messaging"
	"gitlab.com/eukano/perseus/signing"
	"gitlab.com/eukano/perseus/trust"
	"golang.org/x/xerrors"
)

func FindConfiguration() (*Configuration, error) {
	var c Configuration
	return &c, configure.Find("agent.toml", &c)
}

func LoadConfiguration(path string) (*Configuration, error) {
	var c Configuration
	return &c, configure.Load(path, &c)
}

func SaveConfiguration(path string, config *Configuration) error {
	return configure.Save(path, config)
}

type Configuration struct {
	configure.ConfigFile

	Subject         *trust.SubjectConfiguration  `toml:"subject,omitempty"`
	Trust           *trust.Configuration         `toml:"trust,omitempty"`
	Announce        *AnnouncementConfiguration   `toml:"announce,omitempty"`
	Brokers         []*BrokerConfiguration       `toml:"brokers,omitempty"`
	Builders        builder.ClientConfigurations `toml:"builders,omitempty"`
	SignatureStores signing.ConfigurationSet     `toml:"signature-stores,omitempty"`
}

type AnnouncementConfiguration struct {
	Connect *connect.Configuration `toml:"connect,omitempty"`
}

type BrokerConfiguration struct {
	messaging.BrokerConfiguration

	Topic string `toml:"topic,omitempty"`
}

func (c *Configuration) Save() error {
	return SaveConfiguration(c.LoadPath(), c)
}

func (c *Configuration) Validate() error {
	if c.Subject == nil {
		c.Subject = new(trust.SubjectConfiguration)
	}
	if err := c.Subject.Validate(true); err != nil {
		return err
	}

	if c.Trust == nil {
		c.Trust = new(trust.Configuration)
	}
	if err := c.Trust.Validate(c.Subject.Get(), c.TrustOptions()); err != nil {
		return err
	}

	if c.Announce == nil {
		c.Announce = new(AnnouncementConfiguration)
	}
	if err := c.Announce.Validate(); err != nil {
		return err
	}

	if len(c.Brokers) == 0 {
		return xerrors.Errorf("at least one broker must be defined")
	}
	for _, b := range c.Brokers {
		if err := b.Validate(); err != nil {
			return err
		}
	}

	for _, b := range c.Builders {
		if err := b.Validate(); err != nil {
			return err
		}
	}

	if len(c.SignatureStores) == 0 {
		c.SignatureStores = signing.ConfigurationSet{}
	}
	if err := c.SignatureStores.Validate(); err != nil {
		return err
	}

	return nil
}

func (c *AnnouncementConfiguration) Validate() error {
	if c.Connect == nil {
		c.Connect = new(connect.Configuration)
	}
	if err := c.Connect.Validate(); err != nil {
		return err
	}

	return nil
}
